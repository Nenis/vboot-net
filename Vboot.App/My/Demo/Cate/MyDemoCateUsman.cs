﻿namespace Vboot.App.My;

/// <summary>
/// DEMO分类可使用者
/// </summary>
[SugarTable("my_demo_cate_usman", TableDescription = "分类可使用者")]
public class MyDemoCateUsman
{
    [SugarColumn(IsPrimaryKey = true,ColumnDescription = "DEMO ID")]
    public string mid{ get; set; }
        
    [SugarColumn(IsPrimaryKey = true,ColumnDescription = "可使用者ID")]
    public string oid{ get; set; }
}