﻿using Newtonsoft.Json;

namespace Vboot.App.My;

public class MyDemoCateVo
{
    public DateTime? crtim { get; set; }
    
    public DateTime? uptim { get; set; }
    
    public string crman { get; set; }

    public string upman { get; set; }

    public string notes { get; set; }
    
    [SugarColumn(ColumnDescription = "Id主键", IsPrimaryKey = true)]
    public string id { get; set; }

    public string name { get; set; }
    
    [JsonIgnore] public string pid { get; set; }
    
    [SugarColumn(IsIgnore = true)] public List<MyDemoCateVo> children{ get; set; }
}