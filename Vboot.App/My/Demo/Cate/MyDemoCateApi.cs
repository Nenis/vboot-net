﻿namespace Vboot.App.My;

/// <summary>
/// DEMO分类管理
/// </summary>
public class MyDemoCateApi : IDynamicApiController
{
    private readonly MyDemoCateService _service;

    public MyDemoCateApi(MyDemoCateService service)
    {
        _service = service;
    }

    /// <summary>
    /// 获取所有分类-用于分类选择下拉框
    /// </summary>
    [QueryParameters]
    public async Task<dynamic> GetTreea()
    {
        var trees =await  _service.repo.Context.SqlQueryable<Ztree>
                ("select id,pid,name,fomod as type from my_demo_cate order by ornum")
            .ToTreeAsync(it => it.children, it => it.pid, null);
        return trees;
    }

    /// <summary>
    /// 获取除自身及子节点外的分类
    /// </summary>
    [QueryParameters]
    public async Task<dynamic> GetTreez(string id)
    {
        // var trees =await  _service.repo.Context.SqlQueryable<Ztree>
        //         ("select id,pid,name,fomod as type from my_demo_cate order by ornum")
        //     .ToTreeAsync(it => it.children, it => it.pid, null);
        var treeList = await _service.repo.Context
            .Queryable<MyDemoCate>()
            .WhereIF(!string.IsNullOrWhiteSpace(id), t => t.id != id)
            .OrderBy(it => it.ornum)
            .ToTreeAsync(it => it.children, it => it.pid, null);
        return treeList;
    }

    /// <summary>
    /// 获取分类treeTable数据
    /// </summary>
    [QueryParameters]
    public async Task<dynamic> GetTree(string name)
    {
        Sqler sqler = new Sqler("my_demo_cate");
        sqler.addLike("t.name", name);
        sqler.addOrder("t.ornum");
        sqler.addWhere("t.avtag=1");
        sqler.addSelect("case when t.pid  IS NULL then '0' else t.pid end pid,t.notes");
        sqler.selectCUinfo();
        
        var trees =await  _service.repo.Context.SqlQueryable<MyDemoCateVo>(sqler.getSql())
            .ToTreeAsync(it => it.children, it => it.pid, null);
        return trees;
        
        // var trees =await _service.repo.Context.SqlQueryable<MyDemoCateVo>(sqler.getSql())
        //     .ToListAsync();
        // return TreeUtils.BulidTree(trees);
    }

    /// <summary>
    /// 获取单个分类详细信息
    /// </summary>
    public async Task<MyDemoCate> GetOne(string id)
    {
        return await _service.Select(id);
    }

    /// <summary>
    /// 新增DEMO分类
    /// </summary>
    public async Task Post(MyDemoCate cate)
    {
        await _service.Insertx(cate);
    }

    /// <summary>
    /// 更新DEMO分类
    /// </summary>
    public async Task Put(MyDemoCate cate)
    {
        await _service.Update(cate);
    }

    /// <summary>
    /// 删除DEMO分类
    /// </summary>
    public async Task Delete(string ids)
    {
        var idArr = ids.Split(",");
        foreach (var id in idArr)
        {
            var count = await
                _service.repo.Context.Queryable<MyDemoCate>().Where(it => it.pid == id).CountAsync();
            if (count > 0)
            {
                throw new Exception("有子分类无法删除");
            }
        }

        await _service.DeleteAsync(ids);
    }
}