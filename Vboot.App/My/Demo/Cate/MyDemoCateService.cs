﻿using Vboot.Core.Module.Bpm;

namespace Vboot.App.My;

//DEMO分类服务
public class MyDemoCateService : BaseCateService<MyDemoCate>, ITransient
{
    public async Task Insertx(MyDemoCate cate)
    {
        BpmProcTmpl bpmProcTmpl = new BpmProcTmpl();
        bpmProcTmpl.name = cate.name;
        bpmProcTmpl.crman = cate.crman;
        bpmProcTmpl.crtim = DateTime.Now;
        bpmProcTmpl.orxml = cate.prxml;
        if (!string.IsNullOrEmpty(bpmProcTmpl.orxml))
        {
            bpmProcTmpl.chxml = "<?xml version=\"1.0\" encoding=\"gb2312\"?>"
                                + "\n<process" + bpmProcTmpl.orxml.Split("bpmn2:process")[1]
                                    .Replace("bpmn2:", "").Replace("activiti:", "") + "process>";
        }
        bpmProcTmpl.id = YitIdHelper.NextId() + "";
        await repo.Context.Insertable(bpmProcTmpl).ExecuteCommandAsync();
        cate.protd = bpmProcTmpl.id;
        await Insert(cate);
    }
    
    public async Task Insert(MyDemoCate cate)
    {
        cate.avtag = true;
        cate.id = YitIdHelper.NextId() + "";
        cate.crmid = XuserUtil.getUserId();
        cate.crtim = DateTime.Now;
        // await repo.Context.Insertable(cate).ExecuteCommandAsync();
        await repo.Context.InsertNav(cate)
            .Include(it => it.remen)
            .Include(it => it.usmen)
            .ExecuteCommandAsync();
    }

    public async Task Updatex(MyDemoCate cate)
    {
        var bpmProcTmpl = await repo.Context.Queryable<BpmProcTmpl>()
            .Where(it => it.id == cate.protd).FirstAsync();
        bpmProcTmpl.orxml = cate.prxml;
        if (!string.IsNullOrEmpty(bpmProcTmpl.orxml))
        {
            bpmProcTmpl.chxml = "<?xml version=\"1.0\" encoding=\"gb2312\"?>"
                                + "\n<process" + bpmProcTmpl.orxml.Split("bpmn2:process")[1]
                                    .Replace("bpmn2:", "").Replace("activiti:", "") + "process>";
        }
        await repo.Context.Updateable(bpmProcTmpl).ExecuteCommandAsync();
        await Update(cate);
    }
    
    public async Task Update(MyDemoCate cate)
    {
        Console.WriteLine(cate);
        cate.upmid = XuserUtil.getUserId();
        cate.uptim = DateTime.Now;
        await repo.Context.Updateable(cate).ExecuteCommandAsync();
        await repo.Context.UpdateNav(cate)
            .Include(it => it.usmen)
            .Include(it => it.remen)
            .ExecuteCommandAsync();
    }
    
    //级联查询
    public async Task<MyDemoCate> Select(string id)
    {
        var cate = await repo.Context.Queryable<MyDemoCate>()
            .Where(it => it.id == id)
            .Includes(t => t.crman)
            .Includes(t => t.upman)
            .Includes(t => t.remen)
            .Includes(t => t.usmen)
            .SingleAsync();
        if (cate.pid != null)
        {
            cate.pname = await repo.Context.Queryable<MyDemoCate>()
                .Where(it => it.id == cate.pid).Select(it => it.name).SingleAsync();
        }
        if (cate.protd != null)
        {
            cate.prxml = await repo.Context.Queryable<BpmProcTmpl>()
                .Where(it => it.id == cate.protd).Select(it => it.orxml).SingleAsync();
        }
        return cate;
    }

    
    public MyDemoCateService(SqlSugarRepository<MyDemoCate> repo)
    {
        this.repo = repo;
    }
}