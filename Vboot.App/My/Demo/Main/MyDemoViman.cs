﻿namespace Vboot.App.My;

/// <summary>
/// DEMO可编辑者映射
/// </summary>
[SugarTable("my_demo_viman", TableDescription = "DEMO可查看者")]
public class MyDemoViman
{
    [SugarColumn(IsPrimaryKey = true,ColumnDescription = "DEMO ID")]
    public string mid{ get; set; }
        
    [SugarColumn(IsPrimaryKey = true,ColumnDescription = "可查看者ID")]
    public string oid{ get; set; }
}