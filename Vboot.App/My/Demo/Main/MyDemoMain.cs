﻿using Vboot.Core.Module.Bpm;

namespace Vboot.App.My;

/// <summary>
/// DEMO主数据
/// </summary> 
[SugarTable("my_demo_main", TableDescription = "DEMO主数据")]
public class MyDemoMain : BaseMainEntity
{
    //关联分类表示例  
    /// <summary>
    /// 分类ID
    /// </summary> 
    [SugarColumn( ColumnDescription = "分类ID", IsNullable = true, Length = 36)]
    public string catid { get; set; }

    //数据字典示例
    /// <summary>
    /// DEMO等级
    /// </summary> 
    [SugarColumn(ColumnDescription = "等级", IsNullable = true, Length = 32)]
    public string grade { get; set; }

    //流水号示例
    /// <summary>
    /// 流水号
    /// </summary> 
    [SugarColumn(ColumnDescription = "流水号", IsNullable = true, Length = 32)]
    public string senum { get; set; }

    //ManyToOne示例
    /// <summary>
    /// 经办人ID
    /// </summary>
    [SugarColumn(ColumnName = "opman", ColumnDescription = "经办人ID", IsNullable = true, Length = 32)]
    public string opmid { get; set; }

    /// <summary>
    /// 经办人
    /// </summary>
    [Navigate(NavigateType.OneToOne, nameof(opmid))]
    public SysOrg opman { get; set; }

    //ManyToMany示例
    /// <summary>
    /// 可查看者
    /// </summary>
    // [SugarColumn(IsIgnore = true)]
    [Navigate(typeof(MyDemoViman), nameof(MyDemoViman.mid), nameof(MyDemoViman.oid))]
    public List<SysOrg> vimen { get; set; } 

    //ManyToMany示例
    /// <summary>
    /// 可编辑者
    /// </summary>
    // [SugarColumn(IsIgnore = true)]
    [Navigate(typeof(MyDemoEdman), nameof(MyDemoEdman.mid), nameof(MyDemoEdman.oid))]
    public List<SysOrg> edmen { get; set; }

    //OneToMany示例
    /// <summary>
    /// 联系人
    /// </summary>
    [Navigate(NavigateType.OneToMany, nameof(MyDemoItem.maiid))]
    [SugarColumn(IsIgnore = true)]
    public List<MyDemoItem> items { get; set; }
    
    //OneToMany附件
    /// <summary>
    /// 附件
    /// </summary>
    [Navigate(NavigateType.OneToMany, nameof(MyDemoAtt.busid))]
    [SugarColumn(IsIgnore = true)]
    public List<MyDemoAtt> atts { get; set; }

    //地址选择示例，可根据实际情况存想要的字段
    //region-----地址相关信息-----
    /// <summary>
    /// 完整地址
    /// </summary>
    [SugarColumn(ColumnDescription = "完整地址", IsNullable = true, Length = 255)]
    public string addre { get; set; }

    /// <summary>
    /// 省市区
    /// </summary>
    [SugarColumn(ColumnDescription = "省市区", IsNullable = true, Length = 64)]
    public string adreg { get; set; }

    /// <summary>
    /// 省市区以外的详细信息
    /// </summary> 
    [SugarColumn(ColumnDescription = "省市区以外的详细信息", IsNullable = true, Length = 128)]
    public string addet { get; set; }

    /// <summary>
    /// 经纬度
    /// </summary> 
    [SugarColumn(ColumnDescription = "经纬度", IsNullable = true, Length = 32)]
    public string adcoo { get; set; }

    /// <summary>
    /// 省
    /// </summary> 
    [SugarColumn(ColumnDescription = "省", IsNullable = true, Length = 32)]
    public string adpro { get; set; }

    /// <summary>
    /// 市
    /// </summary> 
    [SugarColumn(ColumnDescription = "市", IsNullable = true, Length = 32)]
    public string adcit { get; set; }

    /// <summary>
    /// 区
    /// </summary> 
    [SugarColumn(ColumnDescription = "区", IsNullable = true, Length = 32)]
    public string addis { get; set; }
    //endregion
    
    [SugarColumn(ColumnDescription = "全局流程模板ID", IsNullable = true, Length = 32)]
    public string protd { get; set; }
    
    [SugarColumn(IsIgnore = true)] public string prxml { get; set; }

    [SugarColumn(IsIgnore = true)] 
    public Zbpm zbpm { get; set; }
    
    [SugarColumn(ColumnDescription = "状态", IsNullable = true, Length = 8)]
    public string state { get; set; }
}