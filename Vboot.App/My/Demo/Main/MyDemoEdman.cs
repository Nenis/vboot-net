﻿namespace Vboot.App.My;

/// <summary>
/// DEMO可编辑者映射
/// </summary>
[SugarTable("my_demo_edman", TableDescription = "DEMO可编辑者")]
public class MyDemoEdman
{
    [SugarColumn(IsPrimaryKey = true,ColumnDescription = "DEMO ID", Length = 32)]
    public string mid{ get; set; }
        
    [SugarColumn(IsPrimaryKey = true,ColumnDescription = "可编辑者ID", Length = 32)]
    public string oid{ get; set; }
}