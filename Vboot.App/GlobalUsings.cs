﻿global using Vboot.Core.Common;
global using Vboot.Core.Framework;
global using Vboot.Core.Module.Sys;
global using Vboot.Core.Module.Ass;

global using Furion.DependencyInjection;
global using Furion.DynamicApiController;

global using SqlSugar;
global using System;
global using System.Collections.Generic;
global using System.Linq;
global using System.Threading.Tasks;

global using Yitter.IdGenerator;


