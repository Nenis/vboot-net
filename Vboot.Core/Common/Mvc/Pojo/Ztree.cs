﻿using Newtonsoft.Json;

namespace Vboot.Core.Common;

//树形pojo
public class Ztree
{
    [SugarColumn(ColumnDescription = "Id主键", IsPrimaryKey = true)]
    public string id { get; set; }

    public string name { get; set; }
    
    public string type { get; set; }

    [JsonIgnore] public string pid { get; set; }
    
    [SugarColumn(IsIgnore = true)] public List<Ztree> children{ get; set; }
    
    
}