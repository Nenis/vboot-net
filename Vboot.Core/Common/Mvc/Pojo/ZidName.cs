﻿namespace Vboot.Core.Common;

//基础pojo，只有id与name属性
public class ZidName
{
    [SugarColumn(ColumnDescription = "Id主键", IsPrimaryKey = true)]
    public string id { get; set; }

    public string name { get; set; }
}