﻿namespace Vboot.Core.Common;

//id,name,pid简单属性pojo
public class ZidNamePid
{
    public string id { get; set; }

    public string name { get; set; }
    
    public string pid { get; set; }
}