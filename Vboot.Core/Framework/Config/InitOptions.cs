﻿namespace Vboot.Core.Framework;

public sealed class InitOptions : IConfigurableOptions
{
    public string UiType  { get; set; }
    
    public string OrgName  { get; set; }
    
    public string OrgRootId  { get; set; }
    
    public string OrgType  { get; set; }
    
}