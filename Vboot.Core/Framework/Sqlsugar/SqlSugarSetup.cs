namespace Vboot.Core.Framework;

//此写法从Admin.Net学习拷贝
public static class SqlSugarSetup
{
    /// <summary>
    /// Sqlsugar上下文初始化
    /// </summary>
    public static void AddSqlSugarSetup(this IServiceCollection services, IConfiguration configuration)
    {
        // SqlSugarScope用AddSingleton单例
        services.AddSingleton<ISqlSugarClient>(provider =>
        {
            var dbOptions = App.GetOptions<ConnectionStringsOptions>();
            var configureExternalServices = new ConfigureExternalServices
            {
                EntityService = (type, column) =>
                {
                    if (column.DataType == "varchar(max)")
                    {
                        if (dbOptions.ConnectionConfigs[0].DbType == SqlSugar.DbType.MySql)
                        {
                            column.DataType = "longtext";
                        }else if (dbOptions.ConnectionConfigs[0].DbType == SqlSugar.DbType.Oracle)
                        {
                            column.DataType = "clob";
                        }
                    }
                }
            };
            dbOptions.ConnectionConfigs.ForEach(config =>
            {
                config.ConfigureExternalServices = configureExternalServices;
            });

            SqlSugarScope sqlSugar = new(dbOptions.ConnectionConfigs, db =>
            {
                dbOptions.ConnectionConfigs.ForEach(config =>
                {
                    var dbProvider = db.GetConnectionScope((string) config.ConfigId);

                    // 设置超时时间
                    dbProvider.Ado.CommandTimeOut = 30;

                    var appOptions = App.GetOptions<AppOptions>();
                    if (appOptions.ShowSql)
                    {
                        // 打印SQL语句
                        dbProvider.Aop.OnLogExecuting = (sql, pars) =>
                        {
                        
                            if (sql.StartsWith("SELECT", StringComparison.OrdinalIgnoreCase))
                                Console.ForegroundColor = ConsoleColor.Green;
                            if (sql.StartsWith("UPDATE", StringComparison.OrdinalIgnoreCase) ||
                                sql.StartsWith("INSERT", StringComparison.OrdinalIgnoreCase))
                                Console.ForegroundColor = ConsoleColor.White;
                            if (sql.StartsWith("DELETE", StringComparison.OrdinalIgnoreCase))
                                Console.ForegroundColor = ConsoleColor.Blue;
                            Console.WriteLine("【" + DateTime.Now + "——执行SQL】\r\n" +
                                              UtilMethods.GetSqlString(SqlSugar.DbType.MySql, sql, pars) + "\r\n");
                            App.PrintToMiniProfiler("SqlSugar", "Info",
                                sql + "\r\n" +
                                db.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value)));
                        };
                    }
                    dbProvider.Aop.OnError = (ex) =>
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        var pars = db.Utilities.SerializeObject(
                            ((SugarParameter[]) ex.Parametres).ToDictionary(it => it.ParameterName, it => it.Value));
                        Console.WriteLine("【" + DateTime.Now + "——错误SQL】\r\n" +
                                          UtilMethods.GetSqlString(SqlSugar.DbType.MySql, ex.Sql,
                                              (SugarParameter[]) ex.Parametres) + "\r\n");
                        App.PrintToMiniProfiler("SqlSugar", "Error",
                            $"{ex.Message}{Environment.NewLine}{ex.Sql}{pars}{Environment.NewLine}");
                    };
                });
            });

            return sqlSugar;
        });
        services.AddScoped(typeof(SqlSugarRepository<>)); // 注册仓储
    }
}