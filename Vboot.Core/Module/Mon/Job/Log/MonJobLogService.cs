﻿namespace Vboot.Core.Module.Mon;

public class MonJobLogService : ITransient
{
    public SqlSugarRepository<MonJobLog> repo { get; }

    public MonJobLogService(SqlSugarRepository<MonJobLog> repo)
    {
        this.repo = repo;
    }

    public async Task<MonJobLog> SingleAsync(string id)
    {
        return await repo.GetSingleAsync(t => t.id == id);
    }

    public async Task DeleteAsync(string[] ids)
    {
        await repo.Context.Deleteable<MonJobLog>().In(ids).ExecuteCommandAsync();
    }
}