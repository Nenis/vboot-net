﻿namespace Vboot.Core.Module.Mon;

[ApiDescriptionSettings("Mon", Tag = "操作日志")]
public class MonLogOperApi : IDynamicApiController
{
    private readonly SqlSugarRepository<MonLogOper> _repo;

    public MonLogOperApi(SqlSugarRepository<MonLogOper> repo)
    {
        _repo = repo;
    }

    [QueryParameters]
    public async Task<dynamic> Get(string name)
    {
        var pp = XreqUtil.GetPp();
        var items = await _repo.Context.Queryable<MonLogOper>()
            .WhereIF(!string.IsNullOrWhiteSpace(name), t => t.name.Contains(name.Trim()))
            .OrderBy(t=>t.crtim,OrderByType.Desc)
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }

    public async Task<MonLogOper> GetOne(string id)
    {
        return await _repo.GetSingleAsync(t => t.id == id);
    }

    public async Task Delete(string ids)
    {
       
        var idArr = ids.Split(",");
        await _repo.Context.Deleteable<MonLogOper>().In(idArr).ExecuteCommandAsync();
    }

    public async Task DeleteAll()
    {
        await _repo.Context.Deleteable<MonLogOper>().ExecuteCommandAsync();
    }
}