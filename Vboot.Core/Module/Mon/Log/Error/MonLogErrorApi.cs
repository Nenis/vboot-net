﻿namespace Vboot.Core.Module.Mon;

[ApiDescriptionSettings("Mon", Tag = "错误日志")]
public class MonLogErrorApi : IDynamicApiController
{
    private readonly SqlSugarRepository<MonLogError> _repo;

    public MonLogErrorApi(SqlSugarRepository<MonLogError> repo)
    {
        _repo = repo;
    }

    [QueryParameters]
    public async Task<dynamic> Get(string name)
    {
        var pp = XreqUtil.GetPp();
        var items = await _repo.Context.Queryable<MonLogError>()
            .WhereIF(!string.IsNullOrWhiteSpace(name), t => t.name.Contains(name.Trim()))
            .OrderBy(t=>t.crtim,OrderByType.Desc)
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }

    public async Task<MonLogError> GetOne(string id)
    {
        return await _repo.GetSingleAsync(t => t.id == id);
    }

    public async Task Delete(string ids)
    {
        var idArr = ids.Split(",");
        await _repo.Context.Deleteable<MonLogError>().In(idArr).ExecuteCommandAsync();
    }

    public async Task DeleteAll()
    {
        await _repo.Context.Deleteable<MonLogError>().ExecuteCommandAsync();
    }
}