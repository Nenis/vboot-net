﻿namespace Vboot.Core.Module.Mon;

[ApiDescriptionSettings("Mon", Tag = "登录日志")]
public class MonLogLoginApi : IDynamicApiController
{
    private readonly MonLogLoginService _service;

    public MonLogLoginApi(MonLogLoginService monLogLoginService)
    {
        _service = monLogLoginService;
    }

    [QueryParameters]
    public async Task<dynamic> Get(string name)
    {
        var pp = XreqUtil.GetPp();
        var items = await _service.repo.Context.Queryable<MonLogLogin>()
            .WhereIF(!string.IsNullOrWhiteSpace(name), t => t.name.Contains(name.Trim()))
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }

    public async Task<MonLogLogin> GetOne(string id)
    {
        return await _service.SingleAsync(id);
    }

    public async Task Delete(string ids)
    {
        await _service.DeleteAsync(ids);
    }

    public async Task DeleteAll()
    {
        await _service.repo.Context.Deleteable<MonLogLogin>().ExecuteCommandAsync();
    }
}