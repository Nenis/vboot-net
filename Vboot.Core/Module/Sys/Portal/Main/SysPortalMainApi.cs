﻿namespace Vboot.Core.Module.Sys;

[ApiDescriptionSettings("Sys", Tag = "门户管理-门户")]
public class SysPortalMainApi : IDynamicApiController
{
    private readonly SysPortalMainService _service;

    public SysPortalMainApi(SysPortalMainService service)
    {
        _service = service;
    }
    
    public async Task<dynamic> GetList()
    {
        return await _service.repo.GetListAsync();
    }

    [QueryParameters]
    public async Task<dynamic> Get(string name)
    {
        var pp = XreqUtil.GetPp();
        var items = await _service.repo.Context.Queryable<SysPortalMain>()
            .WhereIF(!string.IsNullOrWhiteSpace(name), t => t.name.Contains(name.Trim()))
            .Select((t) => new {t.id, t.name,t.notes,t.ornum, t.crtim, t.uptim})
            .OrderBy(t=>t.ornum)
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }
    
    public async Task<SysPortalMain> GetOne(string id)
    {
        return await _service.SingleAsync(id);;
    }

    public async Task<string> Post(SysPortalMain main)
    {
        return await _service.InsertAsync(main);
    }

    public async Task<string> Put(SysPortalMain main)
    {
        return await _service.UpdateAsync(main);
    }

    public async Task Delete(string ids)
    {
        await _service.DeleteAsync(ids);
    }
}