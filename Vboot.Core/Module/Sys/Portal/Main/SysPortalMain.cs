﻿namespace Vboot.Core.Module.Sys;

[SugarTable("sys_portal_main", TableDescription = "门户主信息")]
public class SysPortalMain : BaseMainEntity
{
    [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    public int ornum { get; set; }

}