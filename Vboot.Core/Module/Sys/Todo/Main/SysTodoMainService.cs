﻿using Vboot.Core.Module.Bpm;

namespace Vboot.Core.Module.Sys;

public class SysTodoMainService : BaseService<SysTodoMain>, ITransient
{
    public async Task SendTodos(Zbpm zbpm, List<BpmTaskMain> taskList) {
        foreach (var task in taskList)
        {
            if (task.actag) {
                SysTodoMain todo = new SysTodoMain();
                // todo.id = YitIdHelper.NextId() + "";
                todo.id = task.id;
                todo.name = zbpm.prona;
                todo.link = "/#/page/ofmv?id=" + zbpm.proid;
                todo.modid = zbpm.proid;
                await repo.InsertAsync(todo);
                SysOrg sysOrg = await _sysOrgRepo.GetByIdAsync(task.exman);
                if (sysOrg.type == 8) {
                    SysTodoUser todoTarget = new SysTodoUser();
                    todoTarget.id = YitIdHelper.NextId() + "";
                    todoTarget.todid = todo.id;
                    todoTarget.useid = task.exman;
                    await repo.Context.Insertable(todoTarget).ExecuteCommandAsync();

                    Ztodo ztodo = new Ztodo();
                    ztodo.id = todo.id;
                    ztodo.name = todo.name;
                    ztodo.link = todo.link;
                    ztodo.modid = todo.modid;
                    ztodo.taman = todoTarget.useid;
                    ztodo.crman = zbpm.crman;
                    await _eventPublisher.PublishAsync(new ChannelEventSource("Create:SendTodo",ztodo));
                    
                } else if (sysOrg.type == 4) {
                    string sql = "select oid id from sys_org_post_org where pid=@pid";
                    List<string> userIdList = await repo.Context.Ado.SqlQueryAsync<string>(sql,new {pid=task.exman});;
                    foreach (var uid in userIdList)
                    {
                        SysTodoUser todoTarget = new SysTodoUser();
                        todoTarget.id = YitIdHelper.NextId() + "";
                        todoTarget.todid = todo.id;
                        todoTarget.useid = uid;
                        await repo.Context.Insertable(todoTarget).ExecuteCommandAsync();
                        
                        Ztodo ztodo = new Ztodo();
                        ztodo.id = todo.id;
                        ztodo.name = todo.name;
                        ztodo.link = todo.link;
                        ztodo.modid = todo.modid;
                        ztodo.taman = todoTarget.useid;
                        ztodo.crman = zbpm.crman;
                        await _eventPublisher.PublishAsync(new ChannelEventSource("Create:SendTodo",ztodo));
                    }
                }
            }
        }
    }
    
    
    // public async Task SendTodo(Zbpm zbpm, Znode znode)
    // {
    //     SysTodoMain todo = new SysTodoMain();
    //     todo.id = YitIdHelper.NextId() + "";
    //     todo.name = zbpm.prona;
    //     todo.link = "/#/page/ofmv?id=" + zbpm.proid;
    //     todo.modid = zbpm.proid;
    //
    //     SysTodoUser todoTarget = new SysTodoUser();
    //     todoTarget.id = YitIdHelper.NextId() + "";
    //     todoTarget.todid = todo.id;
    //     todoTarget.useid = znode.exmen;
    //     await repo.InsertAsync(todo);
    //     // repo.Context.Insertable(todoTarget).ExecuteCommand();
    //     await repo.Context.Insertable(todoTarget).ExecuteCommandAsync();
    // }
    
    public async Task SendTodo(Zbpm zbpm, string useid)
    {
        SysTodoMain todo = new SysTodoMain();
        todo.id = YitIdHelper.NextId() + "";
        todo.name = zbpm.prona;
        todo.link = "/#/page/ofmv?id=" + zbpm.proid;
        todo.modid = zbpm.proid;

        SysTodoUser todoTarget = new SysTodoUser();
        todoTarget.id = YitIdHelper.NextId() + "";
        todoTarget.todid = todo.id;
        todoTarget.useid = useid;
        await repo.InsertAsync(todo);
        // repo.Context.Insertable(todoTarget).ExecuteCommand();
        await repo.Context.Insertable(todoTarget).ExecuteCommandAsync();
        
        Ztodo ztodo = new Ztodo();
        ztodo.id = todo.id;
        ztodo.name = todo.name;
        ztodo.link = todo.link;
        ztodo.modid = todo.modid;
        ztodo.taman = todoTarget.useid;
        ztodo.crman = zbpm.crman;
        await _eventPublisher.PublishAsync(new ChannelEventSource("Create:SendTodo",ztodo));
    }

    // public async Task DoneTodo(Zbpm zbpm)
    // {
    //     string sql = "select m.id,t.id as todid from sys_todo_main m inner join sys_todo_user t on t.todid=m.id " +
    //                  "where t.useid=@useid and m.modid=@modid";
    //
    //     dynamic map = repo.Context.Ado.SqlQuerySingle<dynamic>(sql, new {useid = zbpm.haman, modid = zbpm.proid});
    //     if (map != null)
    //     {
    //         string todid = map.todid;
    //         await repo.Context.Deleteable<SysTodoUser>().Where(it => it.id == todid).ExecuteCommandAsync();
    //         SysTodoDone done = new SysTodoDone();
    //         done.id = YitIdHelper.NextId() + "";
    //         done.todid = "" + map.id;
    //         done.useid = zbpm.haman;
    //         done.entim = DateTime.Now;
    //         await repo.Context.Insertable(done).ExecuteCommandAsync();
    //     }
    // }
    
    public async Task DoneTodo(Zbpm zbpm)
    {
        string sql = "select t.useid \"useid\",m.id \"todid\",t.id \"tarid\" from sys_todo_main m inner join sys_todo_user t on t.todid=m.id " +
                     "where m.modid=@modid";
        List<dynamic> mapList =await repo.Context.Ado.SqlQueryAsync<dynamic>(sql, new {modid = zbpm.proid});
        foreach (var map in mapList)
        {
            if (zbpm.haman==map.useid)
            {
                string todid = map.todid;
                await repo.Context.Deleteable<SysTodoUser>().Where(it => it.todid == todid).ExecuteCommandAsync();
                SysTodoDone done = new SysTodoDone();
                done.id = YitIdHelper.NextId() + "";
                done.todid = "" + map.todid;
                done.useid = zbpm.haman;
                done.entim = DateTime.Now;
                await repo.Context.Insertable(done).ExecuteCommandAsync();
                
                Ztodo ztodo = new Ztodo();
                ztodo.id = done.todid;
                ztodo.modid = zbpm.proid;
                ztodo.taman = zbpm.haman;
                await _eventPublisher.PublishAsync(new ChannelEventSource("Create:DoneTodo",ztodo));
            }
        }
    }
    
    public async Task DoneTodos(Zbpm zbpm) {
        string sql = "select t.useid \"useid\",m.id \"todid\",t.id as \"tarid\" from sys_todo_main m inner join sys_todo_user t on t.todid=m.id " +
                     "where m.modid=@modid";
        List<dynamic> mapList =await repo.Context.Ado.SqlQueryAsync<dynamic>(sql, new {modid = zbpm.proid});
        foreach (var map in mapList)
        {
            string tarid = map.tarid;
            await repo.Context.Deleteable<SysTodoUser>().Where(it => it.id == tarid).ExecuteCommandAsync();
            
            if (zbpm.haman==map.useid)
            {
                SysTodoDone done = new SysTodoDone();
                done.id = YitIdHelper.NextId() + "";
                done.todid = "" + map.todid;
                done.useid = zbpm.haman;
                done.entim = DateTime.Now;
                await repo.Context.Insertable(done).ExecuteCommandAsync();
              
            }
            
            Ztodo ztodo = new Ztodo();
            ztodo.modid = zbpm.proid;
            await _eventPublisher.PublishAsync(new ChannelEventSource("Create:DoneTodo",ztodo));
        }
    }
    
    public async Task DoneTodosByTaskIds(string[] taskIds) {
        foreach (var taskId in taskIds)
        {
            string delsql = "delete from sys_todo_main where id=@id";
            await repo.Context.Ado.ExecuteCommandAsync(delsql, new {id=taskId});

            string delsql2 = "delete from sys_todo_user where todid=@todid";
            await repo.Context.Ado.ExecuteCommandAsync(delsql2, new {todid=taskId});
        }
    }


    private readonly SqlSugarRepository<SysOrg> _sysOrgRepo;
    
    private readonly IEventPublisher _eventPublisher;
    
    public SysTodoMainService(SqlSugarRepository<SysTodoMain> repo,
        SqlSugarRepository<SysOrg> sysOrgRepo,
        IEventPublisher eventPublisher)
    {
        this.repo = repo;
         _sysOrgRepo = sysOrgRepo;
         _eventPublisher = eventPublisher;
    }
}