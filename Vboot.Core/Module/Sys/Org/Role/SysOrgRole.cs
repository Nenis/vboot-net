﻿namespace Vboot.Core.Module.Sys;

[SugarTable("sys_org_role", TableDescription = "组织架构角色表")]
[Description("组织架构角色表")]
public class SysOrgRole
{
    [SugarColumn(ColumnDescription = "ID", IsPrimaryKey = true, Length = 36)]
    public string id { get; set; }
    
    [SugarColumn(ColumnDescription = "名称", IsNullable = true, Length = 128)]
    public string name { get; set; }   
    
    [SugarColumn(ColumnDescription = "角色树ID", IsNullable = true, Length = 36)]
    public string treid { get; set; }
    
    [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    public int ornum { get; set; }
    
    [SugarColumn(ColumnDescription = "备注", IsNullable = true, Length = 255)]
    public virtual string notes { get; set; }
    
    public SysOrgRole()
    {
    }

    public SysOrgRole(string id)
    {
        this.id = id;
    }

    public SysOrgRole(string id, string name)
    {
        this.id = id;
        this.name = name;
    }
    
    public SysOrgRole(string id, string name,int ornum)
    {
        this.id = id;
        this.name = name;
        this.ornum = ornum;
    }
}