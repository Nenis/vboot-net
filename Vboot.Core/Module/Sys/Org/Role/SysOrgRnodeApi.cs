﻿namespace Vboot.Core.Module.Sys;

[ApiDescriptionSettings("Sys", Tag = "组织架构-角色节点")]
public class SysOrgRnodeApi : IDynamicApiController
{
    private readonly SysOrgRoleNodeService _service;

    public SysOrgRnodeApi(SysOrgRoleNodeService service)
    {
        _service = service;
    }

    [QueryParameters]
    public async Task<dynamic> GetTreea(string treid)
    {
        if (string.IsNullOrEmpty(treid))
        {
            return new List<Ztree>();
        }
        var trees =await  _service.repo.Context.SqlQueryable<Ztree>
                ("select id,pid,name from sys_org_role_node where treid=@treid order by ornum")
            .AddParameters(new [] {new SugarParameter("@treid", treid)})
            .ToTreeAsync(it => it.children, it => it.pid, null);
        return trees;
    }

    [QueryParameters]
    public async Task<dynamic> GetTreez(string id,string treid)
    {
        if (string.IsNullOrEmpty(treid))
        {
            return new List<Ztree>();
        }
        var treeList = await _service.repo.Context
            .Queryable<SysOrgRoleNode>()
            .WhereIF(!string.IsNullOrWhiteSpace(id), t => t.id != id)
            .Where(t => t.treid == treid)
            .OrderBy(it => it.ornum)
            .ToTreeAsync(it => it.children, it => it.pid, null);
        return treeList;
    }

    [QueryParameters]
    public async Task<dynamic> GetTree(string name,string treid)
    {
        if (string.IsNullOrEmpty(treid))
        {
            return new List<Ztree>();
        }
        var treeList = await _service.repo.Context
            .Queryable<SysOrgRoleNode>()
            .Includes(t => t.crman)
            .Includes(t => t.upman)
            .Where(t => t.treid == treid)
            .WhereIF(!string.IsNullOrWhiteSpace(name), t => t.name.Contains(name.Trim()))
            .OrderBy(it => it.ornum)
            .ToTreeAsync(it => it.children, it => it.pid, null);
        return treeList;
    }
    
    [QueryParameters]
    public async Task<dynamic> Get(string name, string pid,string treid)
    {
        if (string.IsNullOrEmpty(treid))
        {
            return RestPageResult.Build(0,new List<ZidName>());
        }
        var pp = XreqUtil.GetPp();
        var expable = Expressionable.Create<SysOrgRoleNode>();
        expable.And(t => t.treid == treid);
        if (!string.IsNullOrWhiteSpace(name))
        {
            expable.And(t => t.name.Contains(name.Trim()));
        }
        else
        {
            if (pid == "")
            {
                expable.And(t => t.pid == null);
            }
            else if (!string.IsNullOrWhiteSpace(pid))
            {
                expable.And(t => t.pid == pid);
            }
        }

        var items = await _service.repo.Context.Queryable<SysOrgRoleNode>()
            .Where(expable.ToExpression())
            .OrderBy(u => u.ornum)
            .Select((t) => new {t.id, t.name, t.notes, t.crtim, t.uptim})
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }

    public async Task<SysOrgRoleNode> GetOne(string id)
    {
        SysOrgRoleNode main=await _service.SingleAsync(id);
        if (!string.IsNullOrEmpty(main.pid))
        {
            main.parent = await _service.repo.Context.Queryable<SysOrgRoleNode>()
                .Where(it => it.id == main.pid).SingleAsync();
        }
        if (!string.IsNullOrEmpty(main.memid))
        {
            main.member = await _service.repo.Context.Queryable<SysOrg>()
                .Where(it => it.id == main.memid).SingleAsync();
        }
        return main;
    }

    public async Task<string> Post(SysOrgRoleNode node)
    {
        int count = await _service.repo.Context.Queryable<SysOrgRoleNode>()
            .Where(t => t.pid == node.pid)
            .Where(t => t.treid == node.treid)
            .CountAsync();
        node.ornum = ++count;
        await _service.Insertx(node);
        return node.id;
    }

    public async Task<string> Put(SysOrgRoleNode cate)
    {
        await _service.Updatex(cate);
        return cate.id;
    }

    public async Task Delete(string ids)
    {
        var idArr = ids.Split(",");
        foreach (var id in idArr)
        {
            var count = await
                _service.repo.Context.Queryable<SysOrgRoleNode>().Where(it => it.pid == id).CountAsync();
            if (count > 0)
            {
                throw new Exception("有子节点无法删除");
            }
        }

        await _service.DeleteAsync(ids);
    }

    /// <summary>
    /// 移动节点
    /// </summary>
    public async Task PostMove(TreeMovePo po)
    {
        await _service.Move(po);
    }
}