﻿namespace Vboot.Core.Module.Sys;

[SugarTable("sys_org_role_node", TableDescription = "组织架构角色节点表")]
public class SysOrgRoleNode : BaseCateEntity
{
    
    [SqlSugar.SugarColumn(IsIgnore = true)]
    public SysOrgRoleNode parent { get; set; }
    
    /// <summary>
    /// 子节点集合
    /// </summary>
    [SugarColumn(IsIgnore = true)] 
    public List<SysOrgRoleNode> children { get; set; }
    
    /// <summary>
    /// 成员ID
    /// </summary>
    [SugarColumn(ColumnName = "memid", ColumnDescription = "成员ID", IsNullable = true, Length = 32)]
    public string memid { get; set; }
    
    /// <summary>
    /// 角色树ID
    /// </summary>
    [SugarColumn(ColumnName = "treid", ColumnDescription = "角色树ID", IsNullable = true, Length = 32)]
    public string treid { get; set; }
    
    /// <summary>
    /// 成员
    /// </summary>
    [Navigate(NavigateType.OneToOne, nameof(memid))]
    public SysOrg member { get; set; }
    
    public SysOrgRoleNode()
    {
    }

    public SysOrgRoleNode(string id)
    {
        this.id = id;
    }
    
}