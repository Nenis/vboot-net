﻿namespace Vboot.Core.Module.Sys;

public class SysOrgGroupService : BaseMainService<SysOrgGroup>, ITransient
{
    public SysOrgGroupService(SqlSugarRepository<SysOrgGroup> repo)
    {
        base.repo = repo;
    }

    public async Task InsertAsync(SysOrgGroup group, List<SysOrgGroupOrg> gmmaps)
    {
        repo.Context.Ado.BeginTran();
        await base.InsertAsync(@group);
        await repo.Context.Insertable(gmmaps).ExecuteCommandAsync();
        await repo.Context.Insertable(new SysOrg {id = group.id, name = group.name,type=16}).ExecuteCommandAsync();
        repo.Context.Ado.CommitTran();
    }


    public async Task UpdateAsync(SysOrgGroup group, List<SysOrgGroupOrg> gmmaps)
    {
        repo.Context.Ado.BeginTran();
        await base.UpdateAsync(@group);
        await repo.Context.Deleteable<SysOrgGroupOrg>().Where(it => it.gid == @group.id).ExecuteCommandAsync();
        await repo.Context.Insertable(gmmaps).ExecuteCommandAsync();
        await repo.Context.Updateable(new SysOrg {id = group.id, name = group.name,type=16})
            .UpdateColumns(it => new {it.name}).ExecuteCommandAsync();
        repo.Context.Ado.CommitTran();
    }
}