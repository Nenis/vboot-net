﻿using Newtonsoft.Json;

namespace Vboot.Core.Module.Sys;

public class SysOrgDeptVo
{
    [SugarColumn(ColumnDescription = "Id主键", IsPrimaryKey = true)]
    public string id { get; set; }

    public string name { get; set; }
    
    
    public DateTime? crtim { get; set; }
    
    public DateTime? uptim { get; set; }
    
    public string notes { get; set; }
    
    [JsonIgnore] public string pid { get; set; }

    [SugarColumn(IsIgnore = true)] public List<SysOrgDeptVo> children { get; set; }
}