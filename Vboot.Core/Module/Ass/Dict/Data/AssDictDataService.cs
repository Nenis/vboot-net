﻿namespace Vboot.Core.Module.Ass;

public class AssDictDataService : BaseMainService<AssDictData>, ITransient
{
    public AssDictDataService(SqlSugarRepository<AssDictData> repo)
    {
        base.repo = repo;
    }
}