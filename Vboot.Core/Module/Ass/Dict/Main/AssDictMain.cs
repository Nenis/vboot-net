﻿namespace Vboot.Core.Module.Ass;

[SugarTable("ass_dict_main", TableDescription = "字典主表")]
[Description("字典主表")]
public class AssDictMain : BaseMainEntity
{
    [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    public int ornum { get; set; }

    [SugarColumn(ColumnDescription = "类型", IsNullable = true, Length = 32)]
    public string catid { get; set; }

}