﻿namespace Vboot.Core.Module.Ass;

[ApiDescriptionSettings("Ass", Tag = "数据字典")]
public class AssDictMainApi : IDynamicApiController
{
    private readonly AssDictMainService _service;

    public AssDictMainApi(AssDictMainService service)
    {
        _service = service;
    }
    
    public async Task<dynamic> GetList()
    {
        return await _service.repo.GetListAsync();
    }

    [QueryParameters]
    public async Task<dynamic> Get( string name)
    {
        var pp = XreqUtil.GetPp();
        var items = await _service.repo.Context.Queryable<AssDictMain>()
            .WhereIF(!string.IsNullOrWhiteSpace(name), t 
                => t.name.Contains(name.Trim())||t.id.Contains(name.Trim()))
            .OrderBy(u => u.ornum)
            .Select((t) => new {t.id, t.name, t.crtim,t.uptim, t.notes})
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }

    public async Task<AssDictMain> GetOne(string id)
    {
        return await _service.SingleAsync(id);;
    }

    public async Task Post(AssDictMain main)
    {
        await _service.InsertAsync(main);
    }

    public async Task Put(AssDictMain main)
    {
        await _service.UpdateAsync(main);
    }

    public async Task Delete(string ids)
    {
        await _service.DeleteAsync(ids);
    }
}