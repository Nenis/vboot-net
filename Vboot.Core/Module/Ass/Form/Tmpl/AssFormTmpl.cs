﻿namespace Vboot.Core.Module.Ass;

[SugarTable("ass_form_tmpl", TableDescription = "表单模板")]
public class AssFormTmpl : BaseMainEntity
{
    
    [SugarColumn(ColumnDescription = "类别", IsNullable = true, Length = 36)]
    public string type { get; set; }
    
    [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    public int ornum { get; set; }
    
    [SugarColumn(ColumnDescription = "版本号", IsNullable = true)]
    public int venum { get; set; }
    
    [SugarColumn(ColumnDescription = "表单字段", ColumnDataType = "varchar(max)", IsNullable = true)]
    public string fjson { get; set; }
    
    [SugarColumn(ColumnDescription = "表单页签", ColumnDataType = "varchar(max)", IsNullable = true)]
    public string tjson { get; set; }
    
    [SugarColumn(ColumnDescription = "表单联动", ColumnDataType = "varchar(max)", IsNullable = true)]
    public string ljson { get; set; }

}