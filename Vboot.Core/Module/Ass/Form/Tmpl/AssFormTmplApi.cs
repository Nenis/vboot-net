﻿using Vboot.Core.Module.Sys;

namespace Vboot.Core.Module.Ass;

[ApiDescriptionSettings("Ass", Tag = "表单模板")]
public class AssFormTmplApi : IDynamicApiController
{
    private readonly AssFormTmplService _service;

    public AssFormTmplApi(AssFormTmplService service)
    {
        _service = service;
    }
    
    [QueryParameters]
    public async Task<List<ZidName>> GetList(string type)
    {
        string sql = "select id,name from ass_form_tmpl where type=@type";
        return await _service.repo.Context.SqlQueryable<ZidName>(sql)
            .AddParameters(new [] {new SugarParameter("@type", type)})
            .ToListAsync();
    }

    [QueryParameters]
    public async Task<dynamic> Get(string type,string name)
    {
        var pp=XreqUtil.GetPp();
        var items = await _service.repo.Context.Queryable<AssFormTmpl,SysOrg,SysOrg> 
            ((t,o,o2)=> new JoinQueryInfos(
                JoinType.Left, o.id == t.crmid,
                JoinType.Left, o2.id == t.upmid))
            .WhereIF(!string.IsNullOrWhiteSpace(type), t => t.type == type)
            .WhereIF(!string.IsNullOrWhiteSpace(name), t => t.name.Contains(name.Trim()))
            .OrderBy(t => t.crtim, OrderByType.Desc)
            .Select((t,o,o2) 
                => new {t.id, t.name, t.crtim, t.uptim,crman=o.name,upman=o2.name,t.notes})
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }
    
    public async Task<AssFormTmpl> GetOne(string id)
    {
        return await _service.SingleAsync(id);;
    }

    public async Task<string> Post(AssFormTmpl main)
    {
        return await _service.InsertAsync(main);
    }

    public async Task<string> Put(AssFormTmpl main)
    {
        return await _service.UpdateAsync(main);
    }

    public async Task Delete(string ids)
    {
        await _service.DeleteAsync(ids);
    }
}