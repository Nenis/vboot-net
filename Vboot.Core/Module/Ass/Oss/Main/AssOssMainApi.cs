﻿using Vboot.Core.Module.Sys;

namespace Vboot.Core.Module.Ass;

[ApiDescriptionSettings("Ass", Tag = "文件存储")]
public class AssOssMainApi : IDynamicApiController
{
    private readonly AssOssMainService _service;

    public AssOssMainApi(AssOssMainService assOssMainService)
    {
        _service = assOssMainService;
    }

    [QueryParameters]
    public async Task<dynamic> Get(string name)
    {
        var pp = XreqUtil.GetPp();
        var items = await _service._repo.Context.Queryable<AssOssMain, AssOssFile, SysOrg>
            ((t, f, o) => new JoinQueryInfos(JoinType.Left, f.id == t.filid,
                JoinType.Left, o.id == t.crmid))
            .Select((t, f, o) => new {t.id, t.name, t.type, t.crtim, f.service, f.path, crman = o.name})
            .WhereIF(!string.IsNullOrWhiteSpace(name), t => t.name.Contains(name.Trim()))
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }

    public async Task<AssOssMain> GetOne(string id)
    {
        var cate = await _service.SingleAsync(id);
        return cate;
    }

    public async Task Delete(string ids)
    {
        var idArr = ids.Split(",");
        await _service.DeleteAsync(idArr);
    }

    [NonUnify]
    public async Task<Zfile> PostUpload(IFormFile file)
    {
        Console.WriteLine(file);
        var sysFile = await _service.UploadFile(file);
        return sysFile;
    }

    [QueryParameters]
    public IActionResult GetDownload(string table, string id)
    {
        return _service.DownloadFile(table, id);
    }

    [QueryParameters]
    [AllowAnonymous]
    public IActionResult GetShow(string token, string id)
    {
        var (isValid, tokenData, validationResult) = JWTEncryption.Validate(token.Substring(7));
        // Console.WriteLine(isValid);
        // Console.WriteLine(tokenData);
        // Console.WriteLine(validationResult);
        if (isValid)
        {
            return _service.DownloadFile(null, id);
        }
        return null;
    }
}