﻿namespace Vboot.Core.Module.Ass;

public class Zfile
{
    public string id { get; set; }

    public string name { get; set; }
    
    public string size { get; set; }
    
    public string path { get; set; }
    
    public string filid { get; set; }
}