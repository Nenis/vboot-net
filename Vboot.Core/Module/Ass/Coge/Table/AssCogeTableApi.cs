﻿using Microsoft.AspNetCore.Hosting;

namespace Vboot.Core.Module.Ass;

[ApiDescriptionSettings("Ass", Tag = "代码生成")]
public class AssCogeTableApi : IDynamicApiController
{
    private readonly AssCogeTableService _service;

    private readonly IWebHostEnvironment _webHostEnvironment;

    private readonly IHttpContextAccessor _httpContextAccessor;

    public AssCogeTableApi(AssCogeTableService service,
        IWebHostEnvironment webHostEnvironment,
        IHttpContextAccessor httpContextAccessor)
    {
        _service = service;
        _httpContextAccessor = httpContextAccessor;
        _webHostEnvironment = webHostEnvironment;
    }

    [QueryParameters]
    public async Task<dynamic> Get(string maiid, string name)
    {
        var pp = XreqUtil.GetPp();
        var items = await _service.repo.Context.Queryable<AssCogeTable>()
            .WhereIF(!string.IsNullOrWhiteSpace(name), t => t.name.Contains(name.Trim()))
            .OrderBy(u => u.ornum)
            .Select((t) => new {t.id, t.name, t.crtim, t.uptim, t.notes})
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        return RestPageResult.Build(pp.total.Value, items);
    }

    [QueryParameters]
    public async Task<dynamic> GetList(string maiid)
    {
        return await _service.repo.Context.Queryable<AssCogeTable>().ToListAsync();
    }

    [QueryParameters]
    public async Task<IActionResult> GetZip(string id)
    {
        GenerateDto dto = new GenerateDto();
        dto.GenTable = await _service.FindOne(id);
        if (!string.IsNullOrEmpty(dto.GenTable.baent))
        {
            dto.GenTable.baser = dto.GenTable.baent.Replace("Entity", "Service");
        }

        //自定义路径
        dto.ZipPath = Path.Combine(_webHostEnvironment.WebRootPath, "vms","zips");
        dto.GenCodePath = Path.Combine(dto.ZipPath, DateTime.Now.ToString("yyyyMMdd"));

        string zipReturnFileName = $"vboot-{DateTime.Now:MMddHHmmss}.zip";

        //生成代码到指定文件夹
        CodeGeneratorTool.Generate(dto);
        FileHelper.ZipGenCode(dto.ZipPath, dto.GenCodePath, zipReturnFileName);
        // Console.WriteLine("path=" + dto.ZipPath);
        // Console.WriteLine("filename=" + zipReturnFileName);
        string zipFileFullName = Path.Combine(dto.ZipPath, zipReturnFileName);
        _httpContextAccessor.HttpContext.Response.Headers.Add("Content-Disposition", 
        $"attachment; download-filename={zipReturnFileName}");
        // _httpContextAccessor.HttpContext.Response.Headers.Add("Access-Control-Expose-Headers", "download-filename");
        _httpContextAccessor.HttpContext.Response.Headers.Add("download-filename", zipReturnFileName);
        return new FileStreamResult(new FileStream(zipFileFullName, FileMode.Open), "application/octet-stream")
            {FileDownloadName = zipReturnFileName};
    }
    
    [QueryParameters]
    public async Task<List<GenCode>> GetShow(string id)
    {
        GenerateDto dto = new GenerateDto();
        dto.GenTable = await _service.FindOne(id);
        if (!string.IsNullOrEmpty(dto.GenTable.baent))
        {
            dto.GenTable.baser = dto.GenTable.baent.Replace("Entity", "Service");
        }

        dto.ZipPath = Path.Combine(_webHostEnvironment.WebRootPath,"vms", "zips");
        dto.GenCodePath = Path.Combine(dto.ZipPath, DateTime.Now.ToString("yyyyMMdd"));

        CodeGeneratorTool.Generate(dto);
        return dto.GenCodes;
    }

    public async Task<AssCogeTable> GetOne(string id)
    {
        return await _service.FindOne(id);
    }

    public async Task Post(AssCogeTable data)
    {
        await _service.Insertx(data);
    }

    [MyUnitOfWork]
    public async Task Put(AssCogeTable data)
    {
        await _service.Updatex(data);
    }

    public async Task Delete(string ids)
    {
        await _service.DeleteAsync(ids);
    }
}