﻿using Vboot.Core.Module.Sys;
namespace Vboot.Core.Module.Gen;

[ApiDescriptionSettings("Gen")]
public class GenOrgGroupApi : IDynamicApiController
{
    private readonly SqlSugarRepository<SysOrgGroup> _repo;

    public GenOrgGroupApi(SqlSugarRepository<SysOrgGroup> repo)
    {
        _repo = repo;
    }
    
    [QueryParameters]
    public async Task<dynamic> GetTree()
    {
        var treeList =await  _repo.Context.SqlQueryable<Ztree>
            ("select id,pid,name,'cate' type from sys_org_group_cate " +
             "union all select id,catid as pid,name,'group' type from sys_org_group")
            .ToTreeAsync(it => it.children, it => it.pid, null);
        return treeList;
    }
    
    [QueryParameters]
    public async Task<dynamic> GetList(string pid,string type,string name)
    {
        if (type == "cate")
        {
            var list =await  _repo.Context.SqlQueryable<ZidName>
                    ("select id,name from sys_org_group where catid=@catid order by ornum")
                .AddParameters(new SugarParameter[] {new SugarParameter("@catid", pid)})
                .ToListAsync();
            return list;
        }else if (type == "group")
        {
            var list =await  _repo.Context.SqlQueryable<ZidName>
                    ("select t.id,t.name from sys_org t inner join sys_org_group_org o on o.oid=t.id where o.gid=@gid")
                .AddParameters(new SugarParameter[] {new SugarParameter("@gid", pid)})
                .ToListAsync();
            return list;
        }
        else
        {
            return new List<ZidName>();
        }
    }
    

}