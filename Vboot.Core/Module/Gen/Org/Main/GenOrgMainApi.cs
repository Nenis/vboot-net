﻿using Furion.Logging;
using Vboot.Core.Module.Sys;
using DbType = SqlSugar.DbType;

namespace Vboot.Core.Module.Gen;

[ApiDescriptionSettings("Gen")]
public class GenOrgMainApi : IDynamicApiController
{
    private readonly SqlSugarRepository<SysOrg> repo;

    public GenOrgMainApi(SqlSugarRepository<SysOrg> Repo)
    {
        repo = Repo;
    }

    //根据部门ID，查询下级所有的部门,岗位,用户
    [QueryParameters]
    public async Task<List<ZidName>> Get(string depid, int type, string name)
    {
        List<ZidName> list = new List<ZidName>();
        if (string.IsNullOrEmpty(name) && string.IsNullOrEmpty(depid))
        {
            return list;
        }

        if ((type & 2) != 0)
        {
            //部门
            Sqler deptSqler = new Sqler("sys_org_dept");
            if (!string.IsNullOrEmpty(name))
            {
                deptSqler.addLike("t.name", name);
            }
            else
            {
                deptSqler.addEqual("t.pid", depid);
            }

            Console.WriteLine(deptSqler.getSql());
            List<ZidName> deptList = await
                repo.Context.Ado.SqlQueryAsync<ZidName>(deptSqler.getSql(), deptSqler.getParams());
            list.AddRange(deptList);
        }

        if ((type & 4) != 0)
        {
            //岗位
            Sqler postSqler = new Sqler("sys_org_post");
            if (!string.IsNullOrEmpty(name))
            {
                postSqler.addLike("t.name", name);
            }
            else
            {
                postSqler.addEqual("t.depid", depid);
            }

            List<ZidName> postList = await
                repo.Context.Ado.SqlQueryAsync<ZidName>(postSqler.getSql(), postSqler.getParams());
            list.AddRange(postList);
        }

        if ((type & 8) != 0)
        {
            //用户
            Sqler userSqler = new Sqler("sys_org_user");
            if (!string.IsNullOrEmpty(name))
            {
                userSqler.addLike("t.name", name);
            }
            else
            {
                userSqler.addEqual("t.depid", depid);
            }

            List<ZidName> userList = await
                repo.Context.Ado.SqlQueryAsync<ZidName>(userSqler.getSql(), userSqler.getParams());
            list.AddRange(userList);
        }

        return list;
    }
    
    //根据组织架构ids获取SysOrg对象数组
    [QueryParameters]
    public async Task<List<ZidName>> GetList(string ids)
    {
        var dbOptions = App.GetOptions<ConnectionStringsOptions>();
        string sql = "select t.id,t.name from sys_org t ";
        if (ids.Contains(";"))
        {
            ids = "'" + ids.Replace(";", "','") + "'";
            sql += " where t.id in (" + ids + ")";
            if (dbOptions.ConnectionConfigs[0].DbType == DbType.MySql)
            {
                sql += " order by field(id," + ids + ")";
            }
            else if (dbOptions.ConnectionConfigs[0].DbType == DbType.SqlServer)
            {
                sql+=" order by CHARINDEX(id,'" + ids.Replace("'","") + "')";
            }else if (dbOptions.ConnectionConfigs[0].DbType == DbType.Oracle)
            {
                sql+=" order by INSTR('" + ids.Replace("'","") + "',id)";
            }
        }
        else
        {
            sql += " where t.id='"+ids+"'";
        }

        Console.WriteLine(sql);
        List<ZidName> orgList = await repo.Context.Ado.SqlQueryAsync<ZidName>(sql);
        return orgList;
    }

    // List<ZidName> deptList = await 
    //     repo.Context.SqlQueryable<ZidName>("select id,name from sys_org_dept")
    //         .Where("name like @name",new{name="%"+name+"%"})
    //         .ToListAsync();
}