﻿using Vboot.Core.Module.Sys;

namespace Vboot.Core.Module.Gen;

[ApiDescriptionSettings("Gen")]
public class GenOrgDeptApi : IDynamicApiController
{
    private readonly SqlSugarRepository<SysOrgDept> repo;

    public GenOrgDeptApi(SqlSugarRepository<SysOrgDept> Repo)
    {
        repo = Repo;
    }

    public List<Ztree> GetTree()
    {
        var trees = repo.Context.SqlQueryable<Ztree>("select id,pid,name from sys_org_dept where avtag=1")
            .ToTreeAsync(it => it.children, it => it.pid, null).Result;
        Console.WriteLine(trees);
        return trees;
    }
}