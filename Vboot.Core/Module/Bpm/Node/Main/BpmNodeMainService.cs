﻿namespace Vboot.Core.Module.Bpm;

public class BpmNodeMainService : ITransient
{
    public async Task<BpmNodeMain> SaveNode(Zbpm zbpm, Znode znode)
    {
        BpmNodeMain node = new BpmNodeMain();
        node.facno = znode.facno;
        node.facna = znode.facna;
        node.facty = znode.facty;
        node.flway = znode.flway;
        node.proid = zbpm.proid;
        node.state = "20";
        node.id = YitIdHelper.NextId() + "";
        await _repo.InsertAsync(node);
        return node;
    }


    public async Task<BpmNodeMain> FindOne(string id)
    {
        return await _repo.GetSingleAsync(t => t.id == id);
    }

    public async Task Delete(string id)
    {
        await _repo.Context.Deleteable<BpmNodeMain>().Where(it => it.id == id).ExecuteCommandAsync();
    }


    private readonly SqlSugarRepository<BpmNodeMain> _repo;

    public BpmNodeMainService(SqlSugarRepository<BpmNodeMain> repo)
    {
        _repo = repo;
    }
}