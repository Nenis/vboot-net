﻿namespace Vboot.Core.Module.Bpm;

public class BpmProcParamService : ITransient
{
    public void Save(BpmProcParam param){
        _repo.Insert(param);
    }

    public async Task Delete(string id) {
      await  _repo.Context.Deleteable<BpmProcParam>().Where(it => it.id == id).ExecuteCommandAsync();
    }
    

    private readonly SqlSugarRepository<BpmProcParam> _repo;

    public BpmProcParamService(SqlSugarRepository<BpmProcParam> repo)
    {
        _repo = repo;
    }
}