﻿namespace Vboot.Core.Module.Bpm;

public class BpmProcTmplService : BaseMainService<BpmProcTmpl>, ITransient
{
    public BpmProcTmplService(SqlSugarRepository<BpmProcTmpl> repo)
    {
        this.repo = repo;
    }
}