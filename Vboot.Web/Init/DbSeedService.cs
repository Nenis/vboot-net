﻿using System.Reflection;
using Furion.DependencyInjection;
using Microsoft.Extensions.Options;
using SqlSugar;
using Vboot.Core.Framework;
using Vboot.Core.Module.Mon;

namespace Vboot.Web.Init;

public class DbSeedService : IScoped
{
    private readonly SqlSugarScope _sqlSugarScope;

    private readonly DbInitListener _dbInitListener;
    
    private readonly MonJobMainService _monJobMainService;
    
    private readonly AppOptions _appOptions;

    public DbSeedService(DbInitListener dbInitListener,
        ISqlSugarClient sqlSugarClient,
        MonJobMainService monJobMainService,
        IOptions<AppOptions> appOptions)
    {
        _dbInitListener = dbInitListener;
        _sqlSugarScope = (SqlSugarScope) sqlSugarClient;
        _monJobMainService = monJobMainService;
        _appOptions = appOptions.Value;
    }

    public async void Init()
    {
        var path = AppDomain.CurrentDomain.RelativeSearchPath ?? AppDomain.CurrentDomain.BaseDirectory;

        // core表自动生成
        if (_appOptions.DbFirstForCore)
        {
            var coreAssemblies = System.IO.Directory.GetFiles(path, "Vboot.Core.dll").Select(Assembly.LoadFrom)
                .ToArray();
        
            var coreModelTypes = coreAssemblies
                .SelectMany(a => a.DefinedTypes)
                .Select(type => type.AsType())
                .Where(x => x.IsClass && x.Namespace != null && x.Namespace.StartsWith("Vboot")).ToList();
            coreModelTypes.ForEach(t =>
            {
                var customAttributeDatas = t.CustomAttributes;
                foreach (var attribute in customAttributeDatas)
                {
                    if (attribute.ToString().Contains("SugarTable"))
                    {
                        var tAtt = t.GetCustomAttribute<TenantAttribute>();
                        var provider = _sqlSugarScope.GetConnectionScope(tAtt == null ? "Vboot" : tAtt.configId);
                        // Console.WriteLine("provider=" + provider);
                        provider.CodeFirst.InitTables(t);
        
        
                        // _repo.Context.CodeFirst.InitTables(t);
                        Console.WriteLine(attribute);
                    }
                }
            });
        }

        
        //extend表自动生成
        if (_appOptions.DbFirstForExtend)
        {
            var extendAssemblies = System.IO.Directory.GetFiles(path, "Vboot.Extend.dll").Select(Assembly.LoadFrom)
                .ToArray();

            var extendModelTypes = extendAssemblies
                .SelectMany(a => a.DefinedTypes)
                .Select(type => type.AsType())
                .Where(x => x.IsClass && x.Namespace != null && x.Namespace.StartsWith("Vboot")).ToList();
            extendModelTypes.ForEach(t =>
            {
                var customAttributeDatas = t.CustomAttributes;
                foreach (var attribute in customAttributeDatas)
                {
                    if (attribute.ToString().Contains("SugarTable"))
                    {
                        var tAtt = t.GetCustomAttribute<TenantAttribute>();
                        var provider = _sqlSugarScope.GetConnectionScope(tAtt == null ? "Vboot" : tAtt.configId);
                        provider.CodeFirst.InitTables(t);
                        // _repo.Context.CodeFirst.InitTables(t);
                        Console.WriteLine(attribute);
                    }
                }
            });
        }

        //app表自动生成
        if (_appOptions.DbFirstForApp)
        {
            var appAssemblies = System.IO.Directory.GetFiles(path, "Vboot.App.dll").Select(Assembly.LoadFrom)
                .ToArray();

            var appModelTypes = appAssemblies
                .SelectMany(a => a.DefinedTypes)
                .Select(type => type.AsType())
                .Where(x => x.IsClass && x.Namespace != null && x.Namespace.StartsWith("Vboot")).ToList();
            appModelTypes.ForEach(t =>
            {
                var customAttributeDatas = t.CustomAttributes;
                foreach (var attribute in customAttributeDatas)
                {
                    if (attribute.ToString().Contains("SugarTable"))
                    {
                        var tAtt = t.GetCustomAttribute<TenantAttribute>();
                        var provider = _sqlSugarScope.GetConnectionScope(tAtt == null ? "Vboot" : tAtt.configId);
                        provider.CodeFirst.InitTables(t);
                        // _repo.Context.CodeFirst.InitTables(t);
                        Console.WriteLine(attribute);
                    }
                }
            });
        }

        await _dbInitListener.Init();
        
        _monJobMainService.StartAllJob();

        // repo.Context.CodeFirst.InitTables(typeof(SysOrg));
    }
    
    
}