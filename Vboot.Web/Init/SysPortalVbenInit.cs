﻿using Furion.DependencyInjection;
using Vboot.Core.Framework;
using Vboot.Core.Module.Sys;

namespace Vboot.Web.Init;

//系统门户与菜单初始化
public class SysPortalVbenInit : ITransient
{
    private readonly SqlSugarRepository<SysPortalMain> _mainRepo;
    
    private readonly SqlSugarRepository<SysPortalMenu> _menuRepo;

    public SysPortalVbenInit(SqlSugarRepository<SysPortalMain> mainRepo,
        SqlSugarRepository<SysPortalMenu> menuRepo)
    {
        _mainRepo = mainRepo;
        _menuRepo = menuRepo;
    }
    
    //门户初始化
    public async Task initPortal() {
        SysPortalMain sysPortalMain=new SysPortalMain();
        sysPortalMain.id="sys";
        sysPortalMain.ornum=0;
        sysPortalMain.name="管理员门户";
        await _mainRepo.InsertAsync(sysPortalMain);
        
        SysPortalMain sysPortalMain2=new SysPortalMain();
        sysPortalMain2.id="sa";
        sysPortalMain2.ornum=1;
        sysPortalMain2.name="营销门户";
        await _mainRepo.InsertAsync(sysPortalMain2);
    }


    //管理员门户的菜单初始化
    public async Task InitSysMenu()
    {
        List<SysPortalMenu> list = new List<SysPortalMenu>();
        
        // SysPortalMenu home = new SysPortalMenu();
        // home.id="Home";
        // home.name="首页";
        // home.code="Home";
        // home.comp="/home/index";
        // home.path="/home";
        // home.ornum=0;
        // home.icon="ele-House";
        // home.avtag=true;
        // home.shtag=true;
        // home.type="M";
        // home.porid="sys";
        // list.Add(home);
        
        //-----------------系统管理--------------------
        SysPortalMenu menu1 = new SysPortalMenu();
        menu1.id="Sys";
        menu1.name="系统管理";
        menu1.code="Sys";
        menu1.comp="LAYOUT";
        menu1.path="/sys";
        menu1.redirect="/sys/org/user";
        menu1.ornum=1;
        menu1.icon="ant-design:setting-outlined";
        menu1.avtag=true;
        menu1.shtag=true;
        menu1.type="D";
        menu1.porid="sys";
        list.Add(menu1);

        SysPortalMenu menu11 = new SysPortalMenu();
        menu11.id="SysOrg";
        menu11.name="组织架构";
        menu11.code="SysOrg";
        menu11.comp="LAYOUT";
        menu11.path="/sys/org";
        menu11.redirect="/sys/org/user";
        menu11.ornum=1;
        menu11.icon="ant-design:partition-outlined";
        menu11.pid="Sys";
        menu11.avtag=true;
        menu11.shtag=true;
        menu11.type="D";
        menu11.porid="sys";
        list.Add(menu11);

        SysPortalMenu menu111 = new SysPortalMenu();
        menu111.id="SysOrgDept";
        menu111.name="部门管理";
        menu111.code="SysOrgDept";
        menu111.path="/sys/org/dept";
        menu111.comp="/sys/org/dept/index";
        menu111.ornum=10;
        menu111.pid="SysOrg";
        menu111.catag=true;
        menu111.avtag=true;
        menu111.shtag=true;
        menu111.type="M";
        menu111.porid="sys";
        list.Add(menu111);
        
        SysPortalMenu menu111a = new SysPortalMenu();
        menu111a.id="SysOrgDeptEdit";
        menu111a.name="部门编辑";
        menu111a.code="SysOrgDeptEdit";
        menu111a.path="/sys/org/dept/edit";
        menu111a.comp="/sys/org/dept/edit";
        menu111a.ornum=11;
        menu111a.pid="SysOrg";
        menu111a.catag=true;
        menu111a.avtag=true;
        menu111a.shtag=false;
        menu111a.type="M";
        menu111a.porid="sys";
        list.Add(menu111a);

        SysPortalMenu menu112 = new SysPortalMenu();
        menu112.id="SysOrgUser";
        menu112.name="用户管理";
        menu112.code="SysOrgUser";
        menu112.path="/sys/org/user";
        menu112.comp="/sys/org/user/index";
        menu112.ornum=20;
        menu112.pid="SysOrg";
        menu112.catag=true;
        menu112.avtag=true;
        menu112.shtag=true;
        menu112.type="M";
        menu112.porid="sys";
        list.Add(menu112);
        
        SysPortalMenu menu112a = new SysPortalMenu();
        menu112a.id="SysOrgUserEdit";
        menu112a.name="用户编辑";
        menu112a.code="SysOrgUserEdit";
        menu112a.path="/sys/org/user/edit";
        menu112a.comp="/sys/org/user/edit";
        menu112a.ornum=21;
        menu112a.pid="SysOrg";
        menu112a.catag=true;
        menu112a.avtag=true;
        menu112a.shtag=false;
        menu112a.type="M";
        menu112a.porid="sys";
        list.Add(menu112a);

        SysPortalMenu menu113 = new SysPortalMenu();
        menu113.id="SysOrgPost";
        menu113.name="岗位管理";
        menu113.code="SysOrgPost";
        menu113.path="/sys/org/post";
        menu113.comp="/sys/org/post/index";
        menu113.ornum=31;
        menu113.pid="SysOrg";
        menu113.catag=true;
        menu113.avtag=true;
        menu113.shtag=true;
        menu113.type="M";
        menu113.porid="sys";
        list.Add(menu113);
        
        SysPortalMenu menu113a = new SysPortalMenu();
        menu113a.id="SysOrgPostEdit";
        menu113a.name="岗位编辑";
        menu113a.code="SysOrgPostEdit";
        menu113a.path="/sys/org/post/edit";
        menu113a.comp="/sys/org/post/edit";
        menu113a.ornum=32;
        menu113a.pid="SysOrg";
        menu113a.catag=true;
        menu113a.avtag=true;
        menu113a.shtag=false;
        menu113a.type="M";
        menu113a.porid="sys";
        list.Add(menu113a);

        SysPortalMenu menu114 = new SysPortalMenu();
        menu114.id="SysOrgGroup";
        menu114.name="群组管理";
        menu114.code="SysOrgGroup";
        menu114.path="/sys/org/group";
        menu114.comp="/sys/org/group/index";
        menu114.ornum = 40;
        menu114.pid="SysOrg";
        menu114.catag=true;
        menu114.avtag=true;
        menu114.shtag=true;
        menu114.type="M";
        menu114.porid="sys";
        list.Add(menu114);
        
        SysPortalMenu menu114a = new SysPortalMenu();
        menu114a.id="SysOrgGroupEdit";
        menu114a.name="群组编辑";
        menu114a.code="SysOrgGroupEdit";
        menu114a.path="/sys/org/group/edit";
        menu114a.comp="/sys/org/group/edit";
        menu114a.ornum=41;
        menu114a.pid="SysOrg";
        menu114a.catag=true;
        menu114a.avtag=true;
        menu114a.shtag=false;
        menu114a.type="M";
        menu114a.porid="sys";
        list.Add(menu114a);
        
        SysPortalMenu menu12 = new SysPortalMenu();
        menu12.id="SysApi";
        menu12.name="接口管理";
        menu12.code="SysApi";
        menu12.comp="LAYOUT";
        menu12.path="/sys/api";
        menu12.redirect="/sys/api/main";
        menu12.ornum=2;
        menu12.icon="ant-design:safety-certificate-outlined";
        menu12.pid="Sys";
        menu12.avtag=true;
        menu12.shtag=true;
        menu12.type="D";
        menu12.porid="sys";
        list.Add(menu12);
        
        SysPortalMenu menu121 = new SysPortalMenu();
        menu121.id="SysApiMain";
        menu121.name="接口清单";
        menu121.code="SysApiMain";
        menu121.path="/sys/api/main";
        menu121.comp="/sys/api/main/index";
        menu121.ornum=10;
        menu121.pid="SysApi";
        menu121.catag=true;
        menu121.avtag=true;
        menu121.shtag=true;
        menu121.type="M";
        menu121.porid="sys";
        list.Add(menu121);
        
        // SysPortalMenu menu121a = new SysPortalMenu();
        // menu121a.id="SysApiMainEdit";
        // menu121a.name="接口编辑";
        // menu121a.code="SysApiMainEdit";
        // menu121a.path="/sys/api/main/edit";
        // menu121a.comp="/sys/api/main/edit";
        // menu121a.ornum=11;
        // menu121a.icon="ele-Tickets";
        // menu121a.pid="SysApi";
        // menu121a.catag=true;
        // menu121a.avtag=true;
        // menu121a.shtag=false;
        // menu121a.type="M";
        // menu121a.porid="sys";
        // list.Add(menu121a);
        //
        SysPortalMenu menu122 = new SysPortalMenu();
        menu122.id="SysApiRole";
        menu122.name="接口角色";
        menu122.code="SysApiRole";
        menu122.path="/sys/api/role";
        menu122.comp="/sys/api/role/index";
        menu122.ornum=20;
        menu122.pid="SysApi";
        menu122.catag=true;
        menu122.avtag=true;
        menu122.shtag=true;
        menu122.type="M";
        menu122.porid="sys";
        list.Add(menu122);
        
        SysPortalMenu menu122a = new SysPortalMenu();
        menu122a.id="SysApiRoleEdit";
        menu122a.name="角色编辑";
        menu122a.code="SysApiRoleEdit";
        menu122a.path="/sys/api/role/edit";
        menu122a.comp="/sys/api/role/edit";
        menu122a.ornum=21;
        menu122a.pid="SysApi";
        menu122a.catag=true;
        menu122a.avtag=true;
        menu122a.shtag=false;
        menu122a.type="M";
        menu122a.porid="sys";
        list.Add(menu122a);
        
        SysPortalMenu menu13 = new SysPortalMenu();
        menu13.id="SysPortal";
        menu13.name="菜单管理";
        menu13.code="SysPortal";
        menu13.comp="LAYOUT";
        menu13.path="/sys/portal";
        menu13.redirect="/sys/portal/main";
        menu13.ornum=3;
        menu13.icon="ant-design:profile-outlined";
        menu13.pid="Sys";
        menu13.avtag=true;
        menu13.shtag=true;
        menu13.type="D";
        menu13.porid="sys";
        list.Add(menu13);
        
        // SysPortalMenu menu131 = new SysPortalMenu();
        // menu131.id="SysPortalMain";
        // menu131.name="门户清单";
        // menu131.code="SysPortalMain";
        // menu131.path="/sys/portal/main";
        // menu131.comp="/sys/portal/main/index";
        // menu131.ornum=10;
        // menu131.pid="SysPortal";
        // menu131.catag=true;
        // menu131.avtag=true;
        // menu131.shtag=true;
        // menu131.type="M";
        // menu131.porid="sys";
        // list.Add(menu131);
        
        // SysPortalMenu menu131a = new SysPortalMenu();
        // menu131a.id="SysPortalMainEdit";
        // menu131a.name="门户编辑";
        // menu131a.code="SysPortalMainEdit";
        // menu131a.path="/sys/portal/main/edit";
        // menu131a.comp="/sys/portal/main/edit";
        // menu131a.ornum=11;
        // menu131a.pid="SysPortal";
        // menu131a.catag=true;
        // menu131a.avtag=true;
        // menu131a.shtag=false;
        // menu131a.type="M";
        // menu131a.porid="sys";
        // list.Add(menu131a);
        
        SysPortalMenu menu132 = new SysPortalMenu();
        menu132.id="SysPortalMenu";
        menu132.name="菜单列表";
        menu132.code="SysPortalMenu";
        menu132.path="/sys/portal/menu";
        menu132.comp="/sys/portal/menu/index";
        menu132.ornum=12;
        menu132.pid="SysPortal";
        menu132.catag=true;
        menu132.avtag=true;
        menu132.shtag=true;
        menu132.type="M";
        menu132.porid="sys";
        list.Add(menu132);

        SysPortalMenu menu132a = new SysPortalMenu();
        menu132a.id="SysPortalMenuEdit";
        menu132a.name="菜单编辑";
        menu132a.code="SysPortalMenuEdit";
        menu132a.path="/sys/portal/menu/edit";
        menu132a.comp="/sys/portal/menu/edit";
        menu132a.ornum=13;
        menu132a.pid="SysPortal";
        menu132a.catag=true;
        menu132a.avtag=true;
        menu132a.shtag=false;
        menu132a.type="M";
        menu132a.porid="sys";
        list.Add(menu132a);
        
        SysPortalMenu menu133 = new SysPortalMenu();
        menu133.id="SysPortalRole";
        menu133.name="菜单角色";
        menu133.code="SysPortalRole";
        menu133.path="/sys/portal/role";
        menu133.comp="/sys/portal/role/index";
        menu133.ornum=14;
        menu133.pid="SysPortal";
        menu133.catag=true;
        menu133.avtag=true;
        menu133.shtag=true;
        menu133.type="M";
        menu133.porid="sys";
        list.Add(menu133);
        
        SysPortalMenu menu133a = new SysPortalMenu();
        menu133a.id="SysPortalRoleEdit";
        menu133a.name="角色编辑";
        menu133a.code="SysPortalRoleEdit";
        menu133a.path="/sys/portal/role/edit";
        menu133a.comp="/sys/portal/role/edit";
        menu133a.ornum=15;
        menu133a.pid="SysPortal";
        menu133a.catag=true;
        menu133a.avtag=true;
        menu133a.shtag=false;
        menu133a.type="M";
        menu133a.porid="sys";
        list.Add(menu133a);
        
        //-----------------监控中心--------------------
        SysPortalMenu menu2 = new SysPortalMenu();
        menu2.id="Mon";
        menu2.name="监控中心";
        menu2.code="Mon";
        menu2.comp="LAYOUT";
        menu2.path="/mon";
        menu2.redirect="/mon/server/main";
        menu2.ornum=2;
        menu2.icon="ant-design:fund-outlined";
        menu2.avtag=true;
        menu2.shtag=true;
        menu2.type="D";
        menu2.porid="sys";
        list.Add(menu2);

        SysPortalMenu menu211 = new SysPortalMenu();
        menu211.id="MonLogLogin";
        menu211.name="登录日志";
        menu211.code="MonLogLogin";
        menu211.path="/mon/log/login";
        menu211.comp="/mon/log/login/index";
        menu211.ornum=211;
        menu211.pid="Mon";
        menu211.catag=true;
        menu211.avtag=true;
        menu211.shtag=true;
        menu211.type="M";
        menu211.porid="sys";
        list.Add(menu211);
        
        SysPortalMenu menu221 = new SysPortalMenu();
        menu221.id="MonLogOper";
        menu221.name="操作日志";
        menu221.code="MonLogOper";
        menu221.path="/mon/log/oper";
        menu221.comp="/mon/log/oper/index";
        menu221.ornum=221;
        menu221.pid="Mon";
        menu221.catag=true;
        menu221.avtag=true;
        menu221.shtag=true;
        menu221.type="M";
        menu221.porid="sys";
        list.Add(menu221);
        
        SysPortalMenu menu231 = new SysPortalMenu();
        menu231.id="MonLogError";
        menu231.name="错误日志";
        menu231.code="MonLogError";
        menu231.path="/mon/log/error";
        menu231.comp="/mon/log/error/index";
        menu231.ornum=231;
        menu231.pid="Mon";
        menu231.catag=true;
        menu231.avtag=true;
        menu231.shtag=true;
        menu231.type="M";
        menu231.porid="sys";
        list.Add(menu231);

        SysPortalMenu menu261 = new SysPortalMenu();
        menu261.id="MonJobMain";
        menu261.name="定时任务";
        menu261.code="MonJobMain";
        menu261.path="/mon/job/main";
        menu261.comp="/mon/job/main/index";
        menu261.ornum=261;
        menu261.pid="Mon";
        menu261.catag=true;
        menu261.avtag=true;
        menu261.shtag=true;
        menu261.type="M";
        menu261.porid="sys";
        list.Add(menu261);

        SysPortalMenu menu262 = new SysPortalMenu();
        menu262.id="MonJobMainEdit";
        menu262.name="任务编辑";
        menu262.code="MonJobMainEdit";
        menu262.path="/mon/job/main/edit";
        menu262.comp="/mon/job/main/edit";
        menu262.ornum=262;
        menu262.pid="Mon";
        menu262.catag=true;
        menu262.avtag=true;
        menu262.shtag=false;
        menu262.type="M";
        menu262.porid="sys";
        list.Add(menu262);

        SysPortalMenu menu263 = new SysPortalMenu();
        menu263.id="MonJobLog";
        menu263.name="任务日志";
        menu263.code="MonJobLog";
        menu263.path="/mon/job/log";
        menu263.comp="/mon/job/log/index";
        menu263.ornum=263;
        menu263.pid="Mon";
        menu263.catag=true;
        menu263.avtag=true;
        menu263.shtag=false;
        menu263.type="M";
        menu263.porid="sys";
        list.Add(menu263);

        // SysPortalMenu menu271 = new SysPortalMenu();
        // menu271.id="MonServerMain";
        // menu271.name="服务监控";
        // menu271.code="MonServerMain";
        // menu271.path="/mon/server/main";
        // menu271.comp="/mon/server/main/show2";
        // menu271.ornum=271;
        // menu271.pid="Mon";
        // menu271.catag=false;
        // menu271.avtag=true;
        // menu271.shtag=true;
        // menu271.type="M";
        // menu271.porid="sys";
        // list.Add(menu271);
        
        //-----------------辅助工具--------------------
        SysPortalMenu menu3 = new SysPortalMenu();
        menu3.id="Ass";
        menu3.name="辅助工具";
        menu3.code="Ass";
        menu3.comp="LAYOUT";
        menu3.path="/ass";
        menu3.redirect="/ass/dict/main";
        menu3.ornum=3;
        menu3.icon="ant-design:tool-outlined";
        menu3.avtag=true;
        menu3.shtag=true;
        menu3.type="D";
        menu3.porid="sys";
        list.Add(menu3);
        
        // SysPortalMenu menu311 = new SysPortalMenu();
        // menu311.id="AssCogeForm";
        // menu311.name="表单设计";
        // menu311.code="AssCogeForm";
        // menu311.path="/ass/coge/form";
        // menu311.comp="/ass/coge/form/index";
        // menu311.ornum=311;
        // menu311.pid="Ass";
        // menu311.catag=false;
        // menu311.avtag=true;
        // menu311.shtag=true;
        // menu311.type="M";
        // menu311.porid="sys";
        // list.Add(menu311);

        // SysPortalMenu menu312 = new SysPortalMenu();
        // menu312.id="AssCogeFormEdit";
        // menu312.name="表单设计编辑";
        // menu312.code="AssCogeFormEdit";
        // menu312.path="/ass/coge/form/edit";
        // menu312.comp="/ass/coge/form/edit";
        // menu312.ornum=312;
        // menu312.pid="Ass";
        // menu312.catag=false;
        // menu312.avtag=true;
        // menu312.shtag=false;
        // menu312.type="M";
        // menu312.porid="sys";
        // list.Add(menu312);

        // SysPortalMenu menu341 = new SysPortalMenu();
        // menu341.id="AssCogeTable";
        // menu341.name="代码生成";
        // menu341.code="AssCogeTable";
        // menu341.path="/ass/coge/table";
        // menu341.comp="/ass/coge/table/index";
        // menu341.ornum=341;
        // menu341.pid="Ass";
        // menu341.catag=false;
        // menu341.avtag=true;
        // menu341.shtag=true;
        // menu341.type="M";
        // menu341.porid="sys";
        // list.Add(menu341);

        // SysPortalMenu menu342 = new SysPortalMenu();
        // menu342.id="AssCogeTableEdit";
        // menu342.name="代码生成";
        // menu342.code="AssCogeTableEdit";
        // menu342.path="/ass/coge/table/edit";
        // menu342.comp="/ass/coge/table/edit";
        // menu342.ornum=342;
        // menu342.pid="Ass";
        // menu342.catag=false;
        // menu342.avtag=true;
        // menu342.shtag=false;
        // menu342.type="M";
        // menu342.porid="sys";
        // list.Add(menu342);

        SysPortalMenu menu351 = new SysPortalMenu();
        menu351.id="AssDictMain";
        menu351.name="数据字典";
        menu351.code="AssDictMain";
        menu351.path="/ass/dict/main";
        menu351.comp="/ass/dict/main/index";
        menu351.ornum=351;
        menu351.pid="Ass";
        menu351.catag=false;
        menu351.avtag=true;
        menu351.shtag=true;
        menu351.type="M";
        menu351.porid="sys";
        list.Add(menu351);

        SysPortalMenu menu352 = new SysPortalMenu();
        menu352.id="AssDictData";
        menu352.name="字典数据";
        menu352.code="AssDictData";
        menu352.path="/ass/dict/data";
        menu352.comp="/ass/dict/data/index";
        menu352.ornum=352;
        menu352.pid="Ass";
        menu352.catag=false;
        menu352.avtag=true;
        menu352.shtag=false;
        menu352.type="M";
        menu352.porid="sys";
        list.Add(menu352);

        SysPortalMenu menu361 = new SysPortalMenu();
        menu361.id="AssNumMain";
        menu361.name="编号策略";
        menu361.code="AssNumMain";
        menu361.path="/ass/num/main";
        menu361.comp="/ass/num/main/index";
        menu361.ornum=361;
        menu361.pid="Ass";
        menu361.catag=false;
        menu361.avtag=true;
        menu361.shtag=true;
        menu361.type="M";
        menu361.porid="sys";
        list.Add(menu361);

        // SysPortalMenu menu371 = new SysPortalMenu();
        // menu371.id="AssAddrMain";
        // menu371.name="省市区县";
        // menu371.code="AssAddrMain";
        // menu371.path="/ass/addr/main";
        // menu371.comp="/ass/addr/main/index";
        // menu371.ornum=371;
        // menu371.pid="Ass";
        // menu371.catag=false;
        // menu371.avtag=true;
        // menu371.shtag=true;
        // menu371.type="M";
        // menu371.porid="sys";
        // list.Add(menu371);
        
        //-----------------数字办公--------------------
        // SysPortalMenu menu4 = new SysPortalMenu();
        // menu4.id="Oa";
        // menu4.name="数字办公";
        // menu4.code="Oa";
        // menu4.comp="LAYOUT";
        // menu4.path="/oa";
        // menu4.redirect="/oa/flow/tmpl";
        // menu4.ornum=4;
        // menu4.icon="ant-design:retweet-outlined";
        // menu4.avtag=true;
        // menu4.shtag=true;
        // menu4.type="D";
        // menu4.porid="sys";
        // list.Add(menu4);
        
        SysPortalMenu menu41 = new SysPortalMenu();
        menu41.id="OaFlow";
        menu41.name="流程管理";
        menu41.code="OaFlow";
        menu41.comp="LAYOUT";
        menu41.path="/oa/flow";
        menu41.redirect="/oa/flow/tmpl";
        menu41.ornum=4;
        menu41.icon="ant-design:retweet-outlined";
        // menu41.pid="Oa";
        menu41.avtag=true;
        menu41.shtag=true;
        menu41.type="D";
        menu41.porid="sys";
        list.Add(menu41);
        
        SysPortalMenu menu411 = new SysPortalMenu();
        menu411.id="OaFlowCate";
        menu411.name="流程分类";
        menu411.code="OaFlowCate";
        menu411.path="/oa/flow/cate";
        menu411.comp="/oa/flow/cate/index";
        menu411.ornum=411;
        menu411.pid="OaFlow";
        menu411.catag=true;
        menu411.avtag=true;
        menu411.shtag=true;
        menu411.type="M";
        menu411.porid="sys";
        list.Add(menu411);
        
        SysPortalMenu menu411a = new SysPortalMenu();
        menu411a.id="OaFlowCateEdit";
        menu411a.name="流程分类编辑";
        menu411a.code="OaFlowCateEdit";
        menu411a.path="/oa/flow/cate/edit";
        menu411a.comp="/oa/flow/cate/edit";
        menu411a.ornum=4111;
        menu411a.pid="OaFlow";
        menu411a.catag=true;
        menu411a.avtag=true;
        menu411a.shtag=false;
        menu411a.type="M";
        menu411a.porid="sys";
        list.Add(menu411a);
        
        SysPortalMenu menu412 = new SysPortalMenu();
        menu412.id="OaFlowTmpl";
        menu412.name="流程模板";
        menu412.code="OaFlowTmpl";
        menu412.path="/oa/flow/tmpl";
        menu412.comp="/oa/flow/tmpl/index";
        menu412.ornum=412;
        menu412.pid="OaFlow";
        menu412.catag=true;
        menu412.avtag=true;
        menu412.shtag=true;
        menu412.type="M";
        menu412.porid="sys";
        list.Add(menu412);
        
        SysPortalMenu menu412a = new SysPortalMenu();
        menu412a.id="OaFlowTmplEdit";
        menu412a.name="流程模板编辑";
        menu412a.code="OaFlowTmplEdit";
        menu412a.path="/oa/flow/tmpl/edit";
        menu412a.comp="/oa/flow/tmpl/edit";
        menu412a.ornum=4121;
        menu412a.pid="OaFlow";
        menu412a.catag=true;
        menu412a.avtag=true;
        menu412a.shtag=false;
        menu412a.type="M";
        menu412a.porid="sys";
        list.Add(menu412a);
        
        SysPortalMenu menu413 = new SysPortalMenu();
        menu413.id="OaFlowMain";
        menu413.name="流程实例";
        menu413.code="OaFlowMain";
        menu413.path="/oa/flow/main";
        menu413.comp="/oa/flow/main/index";
        menu413.ornum=413;
        menu413.pid="OaFlow";
        menu413.catag=true;
        menu413.avtag=true;
        menu413.shtag=true;
        menu413.type="M";
        menu413.porid="sys";
        list.Add(menu413);
        
        SysPortalMenu menu413a = new SysPortalMenu();
        menu413a.id="OaFlowMainEdit";
        menu413a.name="流程实例编辑";
        menu413a.code="OaFlowMainEdit";
        menu413a.path="/oa/flow/main/edit";
        menu413a.comp="/oa/flow/main/edit";
        menu413a.ornum=4131;
        menu413a.pid="OaFlow";
        menu413a.catag=true;
        menu413a.avtag=true;
        menu413a.shtag=false;
        menu413a.type="M";
        menu413a.porid="sys";
        list.Add(menu413a);
        
        SysPortalMenu menu413b = new SysPortalMenu();
        menu413b.id="OaFlowMainView";
        menu413b.name="流程实例查看";
        menu413b.code="OaFlowMainView";
        menu413b.path="/oa/flow/main/view";
        menu413b.comp="/oa/flow/main/view";
        menu413b.ornum=4132;
        menu413b.pid="OaFlow";
        menu413b.catag=true;
        menu413b.avtag=true;
        menu413b.shtag=false;
        menu413b.type="M";
        menu413b.porid="sys";
        list.Add(menu413b);
        
        //-----------------DEMO--------------------
        SysPortalMenu menu8 = new SysPortalMenu();
        menu8.id="My";
        menu8.name="测试样例";
        menu8.code="My";
        menu8.comp="LAYOUT";
        menu8.path="/my";
        menu8.redirect="/my/demo/main";
        menu8.ornum=8;
        menu8.icon="ant-design:trophy-outlined";
        menu8.avtag=true;
        menu8.shtag=true;
        menu8.type="D";
        menu8.porid="sys";
        list.Add(menu8);

        SysPortalMenu menu811 = new SysPortalMenu();
        menu811.id="MyDemoCate";
        menu811.name="DEMO分类";
        menu811.code="MyDemoCate";
        menu811.path="/my/demo/cate";
        menu811.comp="/my/demo/cate/index";
        menu811.ornum=811;
        menu811.pid="My";
        menu811.catag=true;
        menu811.avtag=true;
        menu811.shtag=true;
        menu811.type="M";
        menu811.porid="sys";
        list.Add(menu811);
        
        SysPortalMenu menu812 = new SysPortalMenu();
        menu812.id="MyDemoMain";
        menu812.name="DEMO清单";
        menu812.code="MyDemoMain";
        menu812.path="/my/demo/main";
        menu812.comp="/my/demo/main/index";
        menu812.ornum=812;
        menu812.pid="My";
        menu812.catag=true;
        menu812.avtag=true;
        menu812.shtag=true;
        menu812.type="M";
        menu812.porid="sys";
        list.Add(menu812);
        
        SysPortalMenu menu813 = new SysPortalMenu();
        menu813.id="MyDemoMainEdit";
        menu813.name="DEMO编辑";
        menu813.code="MyDemoMainEdit";
        menu813.path="/my/demo/main/edit";
        menu813.comp="/my/demo/main/edit";
        menu813.ornum=813;
        menu813.pid="My";
        menu813.catag=true;
        menu813.avtag=true;
        menu813.shtag=false;
        menu813.type = "M";
        menu813.porid="sys";
        list.Add(menu813);
        
        SysPortalMenu menu999 = new SysPortalMenu();
        menu999.id="AboutPage";
        menu999.name="关于";
        menu999.code="AboutPage";
        menu999.path="/sys/about";
        menu999.comp="/sys/about/index";
        menu999.ornum=999;
        menu999.icon="simple-icons:about-dot-me";
        menu999.catag=true;
        menu999.avtag=true;
        menu999.shtag=true;
        menu999.type = "M";
        menu999.porid="sys";
        list.Add(menu999);
        
        await _menuRepo.InsertRangeAsync(list);
    }
    
    //营销门户的菜单初始化
     public async Task InitSaMenu()
    {
        List<SysPortalMenu> list = new List<SysPortalMenu>();
        
        SysPortalMenu home = new SysPortalMenu();
        home.id="Sa-Home";
        home.name="营销首页";
        home.code="Home";
        home.comp="/home/sa";
        home.path="/home";
        home.ornum=0;
        home.icon="ele-House";
        home.avtag=true;
        home.shtag=true;
        home.type="M";
        home.porid="sa";
        list.Add(home);
        
        SysPortalMenu menu1 = new SysPortalMenu();
        menu1.id="Sa-Oa";
        menu1.name="日常办公";
        menu1.code="Oa";
        menu1.comp="LAYOUT";
        menu1.path="/oa";
        menu1.redirect="/oa/flow/main";
        menu1.ornum=2;
        menu1.icon="ele-Menu";
        menu1.avtag=true;
        menu1.shtag=true;
        menu1.type="D";
        menu1.porid="sa";
        list.Add(menu1);
        
        SysPortalMenu menu11 = new SysPortalMenu();
        menu11.id="Sa-OaFlowMain";
        menu11.name="流程审批";
        menu11.code="OaFlowMain";
        menu11.path="/oa/flow/main";
        menu11.comp="/oa/flow/main/index";
        menu11.ornum=21;
        // menu11.icon="ele-Tickets";
        menu11.pid="Sa-Oa";
        menu11.catag=true;
        menu11.avtag=true;
        menu11.shtag=true;
        menu11.type="M";
        menu11.porid="sa";
        list.Add(menu11);
        
        await _menuRepo.InsertRangeAsync(list);
    }
}