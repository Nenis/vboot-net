﻿using Furion.DataEncryption;
using Furion.DependencyInjection;
using Vboot.Core.Common;
using Vboot.Core.Framework;
using Vboot.Core.Module.Sys;

namespace Vboot.Web.Init;

//组织架构初始化，可根据配置生成不同的基础数据
public class SysOrgInit : ITransient
{
    private readonly SqlSugarRepository<SysOrgDept> _deptRepo;
    private readonly SqlSugarRepository<SysOrg> _orgRepo;
    private readonly SqlSugarRepository<SysOrgUser> _userRepo;
    private readonly SqlSugarRepository<SysOrgPost> _postRepo;
    private readonly SqlSugarRepository<SysOrgGroup> _groupRepo;
    private readonly SysOrgRoleTreeService _roleTreeService;
    private readonly SqlSugarRepository<SysOrgRoleNode> _roleNodeRepo;
    private string rootId = "1";

    public SysOrgInit(SqlSugarRepository<SysOrgDept> deptRepo,
        SqlSugarRepository<SysOrg> orgRepo,
        SqlSugarRepository<SysOrgUser> userRepo,
        SqlSugarRepository<SysOrgPost> postRepo,
        SqlSugarRepository<SysOrgGroup> groupRepo,
        SysOrgRoleTreeService roleTreeService,
        SqlSugarRepository<SysOrgRoleNode> roleNodeRepo)
    {
        _deptRepo = deptRepo;
        _orgRepo = orgRepo;
        _userRepo = userRepo;
        _postRepo = postRepo;
        _groupRepo = groupRepo;
        _roleTreeService = roleTreeService;
        _roleNodeRepo = roleNodeRepo;
    }

    public async Task initRoot(string id,string name)
    {
        this.rootId = id;
        SysOrgDept root = new SysOrgDept();
        root.id = id;
        root.name = name;
        root.label = "1";
        root.ornum = 0;
        root.avtag = true;
        root.tier = "x"+rootId+"x";
        root.type = 1;
        await _deptRepo.InsertAsync(root);
        await _orgRepo.InsertAsync(new SysOrg(rootId, root.name,root.type));
    }

    //初始化部门
    public async Task initDept()
    {
        var deptList = new List<SysOrgDept>();
        var orgList = new List<SysOrg>();

        SysOrgDept a = new SysOrgDept();
        a.id = "a";
        a.name = "A组_卡厄塞荷";
        a.label = "1";
        a.ornum = 1;
        a.avtag = true;
        a.tier = "x"+rootId+"xax";
        a.type = 1;
        a.pid = rootId;
        deptList.Add(a);
        orgList.Add(new SysOrg(a.id, a.name,a.type));

        SysOrgDept a1 = new SysOrgDept();
        a1.id = "a1";
        a1.name = "卡塔尔";
        a1.label = "1";
        a1.ornum = 1;
        a1.avtag = true;
        a1.tier = "x"+rootId+"xaxa1x";
        a1.type = 1;
        a1.pid = "a";
        deptList.Add(a1);
        orgList.Add(new SysOrg(a1.id, a1.name,a1.type));
        AddPostionDept(deptList, orgList, "a1");


        SysOrgDept a2 = new SysOrgDept();
        a2.id = "a2";
        a2.name = "厄瓜多尔";
        a2.label = "1";
        a2.ornum = 2;
        a2.avtag = true;
        a2.tier = "x"+rootId+"xaxa2x";
        a2.type = 1;
        a2.pid = "a";
        deptList.Add(a2);
        orgList.Add(new SysOrg(a2.id, a2.name,a2.type));
        AddPostionDept(deptList, orgList, "a2");

        SysOrgDept a3 = new SysOrgDept();
        a3.id = "a3";
        a3.name = "塞内加尔";
        a3.label = "1";
        a3.ornum = 3;
        a3.avtag = true;
        a3.tier = "x"+rootId+"xaxa3x";
        a3.type = 1;
        a3.pid = "a";
        deptList.Add(a3);
        orgList.Add(new SysOrg(a3.id, a3.name,a3.type));
        AddPostionDept(deptList, orgList, "a3");

        SysOrgDept a4 = new SysOrgDept();
        a4.id = "a4";
        a4.name = "荷兰";
        a4.label = "1";
        a4.ornum = 4;
        a4.avtag = true;
        a4.tier = "x"+rootId+"xaxa4x";
        a4.type = 1;
        a4.pid = "a";
        deptList.Add(a4);
        orgList.Add(new SysOrg(a4.id, a4.name,a4.type));
        AddPostionDept(deptList, orgList, "a4");

        SysOrgDept b = new SysOrgDept();
        b.id = "b";
        b.name = "B组_英伊美威";
        b.label = "1";
        b.ornum = 2;
        b.avtag = true;
        b.tier = "x"+rootId+"xbx";
        b.type = 1;
        b.pid = rootId;
        deptList.Add(b);
        orgList.Add(new SysOrg(b.id, b.name,b.type));

        SysOrgDept b1 = new SysOrgDept();
        b1.id = "b1";
        b1.name = "英格兰";
        b1.label = "1";
        b1.ornum = 1;
        b1.avtag = true;
        b1.tier = "x"+rootId+"xbxb1x";
        b1.type = 1;
        b1.pid = "b";
        deptList.Add(b1);
        orgList.Add(new SysOrg(b1.id, b1.name,b1.type));
        AddPostionDept(deptList, orgList, "b1");

        SysOrgDept b2 = new SysOrgDept();
        b2.id = "b2";
        b2.name = "伊朗";
        b2.label = "1";
        b2.ornum = 2;
        b2.avtag = true;
        b2.tier = "x"+rootId+"xbxb2x";
        b2.type = 1;
        b2.pid = "b";
        deptList.Add(b2);
        orgList.Add(new SysOrg(b2.id, b2.name,b2.type));
        AddPostionDept(deptList, orgList, "b2");

        SysOrgDept b3 = new SysOrgDept();
        b3.id = "b3";
        b3.name = "美国";
        b3.label = "1";
        b3.ornum = 3;
        b3.avtag = true;
        b3.tier = "x"+rootId+"xbxb3x";
        b3.type = 1;
        b3.pid = "b";
        deptList.Add(b3);
        orgList.Add(new SysOrg(b3.id, b3.name,b3.type));
        AddPostionDept(deptList, orgList, "b3");

        SysOrgDept b4 = new SysOrgDept();
        b4.id = "b4";
        b4.name = "威尔士";
        b4.label = "1";
        b4.ornum = 4;
        b4.avtag = true;
        b4.tier = "x"+rootId+"xbxb4x";
        b4.type = 1;
        b4.pid = "b";
        deptList.Add(b4);
        orgList.Add(new SysOrg(b4.id, b4.name,b4.type));
        AddPostionDept(deptList, orgList, "b4");

        SysOrgDept c = new SysOrgDept();
        c.id = "c";
        c.name = "C组_阿沙墨波";
        c.label = "1";
        c.ornum = 3;
        c.avtag = true;
        c.tier = "x"+rootId+"xcx";
        c.type = 1;
        c.pid = rootId;
        deptList.Add(c);
        orgList.Add(new SysOrg(c.id, c.name,c.type));

        SysOrgDept c1 = new SysOrgDept();
        c1.id = "c1";
        c1.name = "阿根廷";
        c1.label = "1";
        c1.ornum = 1;
        c1.avtag = true;
        c1.tier = "x"+rootId+"xcxc1x";
        c1.type = 1;
        c1.pid = "c";
        deptList.Add(c1);
        orgList.Add(new SysOrg(c1.id, c1.name,c1.type));
        AddPostionDept(deptList, orgList, "c1");

        SysOrgDept c2 = new SysOrgDept();
        c2.id = "c22";
        c2.name = "沙特";
        c2.label = "1";
        c2.ornum = 2;
        c2.avtag = true;
        c2.tier = "x"+rootId+"xcxc22x";
        c2.type = 1;
        c2.pid = "c";
        deptList.Add(c2);
        orgList.Add(new SysOrg(c2.id, c2.name,c2.type));
        AddPostionDept(deptList, orgList, "c22");

        SysOrgDept c3 = new SysOrgDept();
        c3.id = "c3";
        c3.name = "墨西哥";
        c3.label = "1";
        c3.ornum = 3;
        c3.avtag = true;
        c3.tier = "x"+rootId+"xcxc3x";
        c3.type = 1;
        c3.pid = "c";
        deptList.Add(c3);
        orgList.Add(new SysOrg(c3.id, c3.name,c3.type));
        AddPostionDept(deptList, orgList, "c3");

        SysOrgDept c4 = new SysOrgDept();
        c4.id = "c4";
        c4.name = "波兰";
        c4.label = "1";
        c4.ornum = 4;
        c4.avtag = true;
        c4.tier = "x"+rootId+"xcxc4x";
        c4.type = 1;
        c4.pid = "c";
        deptList.Add(c4);
        orgList.Add(new SysOrg(c4.id, c4.name,c4.type));
        AddPostionDept(deptList, orgList, "c4");

        SysOrgDept d = new SysOrgDept();
        d.id = "d";
        d.name = "D组_法澳丹突";
        d.label = "1";
        d.ornum = 4;
        d.avtag = true;
        d.tier = "x"+rootId+"xdx";
        d.type = 1;
        d.pid = rootId;
        deptList.Add(d);
        orgList.Add(new SysOrg(d.id, d.name,d.type));

        SysOrgDept d1 = new SysOrgDept();
        d1.id = "d1";
        d1.name = "法国";
        d1.label = "1";
        d1.ornum = 1;
        d1.avtag = true;
        d1.tier = "x"+rootId+"xdxd1x";
        d1.type = 1;
        d1.pid = "d";
        deptList.Add(d1);
        orgList.Add(new SysOrg(d1.id, d1.name,d1.type));
        AddPostionDept(deptList, orgList, "d1");

        SysOrgDept d2 = new SysOrgDept();
        d2.id = "d2";
        d2.name = "澳大利亚";
        d2.label = "1";
        d2.ornum = 2;
        d2.avtag = true;
        d2.tier = "x"+rootId+"xdxd2x";
        d2.type = 1;
        d2.pid = "d";
        deptList.Add(d2);
        orgList.Add(new SysOrg(d2.id, d2.name,d2.type));
        AddPostionDept(deptList, orgList, "d2");

        SysOrgDept d3 = new SysOrgDept();
        d3.id = "d3";
        d3.name = "丹麦";
        d3.label = "1";
        d3.ornum = 3;
        d3.avtag = true;
        d3.tier = "x"+rootId+"xdxd3x";
        d3.type = 1;
        d3.pid = "d";
        deptList.Add(d3);
        orgList.Add(new SysOrg(d3.id, d3.name,d3.type));
        AddPostionDept(deptList, orgList, "d3");

        SysOrgDept d4 = new SysOrgDept();
        d4.id = "d4";
        d4.name = "突尼斯";
        d4.label = "1";
        d4.ornum = 4;
        d4.avtag = true;
        d4.tier = "x"+rootId+"xdxd4x";
        d4.type = 1;
        d4.pid = "d";
        deptList.Add(d4);
        orgList.Add(new SysOrg(d4.id, d4.name,d4.type));
        AddPostionDept(deptList, orgList, "d4");

        SysOrgDept e = new SysOrgDept();
        e.id = "e";
        e.name = "E组_西哥德日";
        e.label = "1";
        e.ornum = 5;
        e.avtag = true;
        e.tier = "x"+rootId+"xex";
        e.type = 1;
        e.pid = rootId;
        deptList.Add(e);
        orgList.Add(new SysOrg(e.id, e.name,e.type));

        SysOrgDept e1 = new SysOrgDept();
        e1.id = "e1";
        e1.name = "西班牙";
        e1.label = "1";
        e1.ornum = 1;
        e1.avtag = true;
        e1.tier = "x"+rootId+"xexe1x";
        e1.type = 1;
        e1.pid = "e";
        deptList.Add(e1);
        orgList.Add(new SysOrg(e1.id, e1.name,e1.type));
        AddPostionDept(deptList, orgList, "e1");

        SysOrgDept e2 = new SysOrgDept();
        e2.id = "e2";
        e2.name = "哥斯达黎加";
        e2.label = "1";
        e2.ornum = 2;
        e2.avtag = true;
        e2.tier = "x"+rootId+"xexe2x";
        e2.type = 1;
        e2.pid = "e";
        deptList.Add(e2);
        orgList.Add(new SysOrg(e2.id, e2.name,e2.type));
        AddPostionDept(deptList, orgList, "e2");

        SysOrgDept e3 = new SysOrgDept();
        e3.id = "e3";
        e3.name = "德国";
        e3.label = "1";
        e3.ornum = 3;
        e3.avtag = true;
        e3.tier = "x"+rootId+"xexe3x";
        e3.type = 1;
        e3.pid = "e";
        deptList.Add(e3);
        orgList.Add(new SysOrg(e3.id, e3.name,e3.type));
        AddPostionDept(deptList, orgList, "e3");

        SysOrgDept e4 = new SysOrgDept();
        e4.id = "e4";
        e4.name = "日本";
        e4.label = "1";
        e4.ornum = 4;
        e4.avtag = true;
        e4.tier = "x"+rootId+"xexe4x";
        e4.type = 1;
        e4.pid = "e";
        deptList.Add(e4);
        orgList.Add(new SysOrg(e4.id, e4.name,e4.type));
        AddPostionDept(deptList, orgList, "e4");

        SysOrgDept f = new SysOrgDept();
        f.id = "f";
        f.name = "F组_比加摩克";
        f.label = "1";
        f.ornum = 6;
        f.avtag = true;
        f.tier = "x"+rootId+"xfx";
        f.type = 1;
        f.pid = rootId;
        deptList.Add(f);
        orgList.Add(new SysOrg(f.id, f.name,f.type));

        SysOrgDept f1 = new SysOrgDept();
        f1.id = "f1";
        f1.name = "比利时";
        f1.label = "1";
        f1.ornum = 1;
        f1.avtag = true;
        f1.tier = "x"+rootId+"xfxf1x";
        f1.type = 1;
        f1.pid = "f";
        deptList.Add(f1);
        orgList.Add(new SysOrg(f1.id, f1.name,f1.type));
        AddPostionDept(deptList, orgList, "f1");

        SysOrgDept f2 = new SysOrgDept();
        f2.id = "f2";
        f2.name = "加拿大";
        f2.label = "1";
        f2.ornum = 2;
        f2.avtag = true;
        f2.tier = "x"+rootId+"xfxf2x";
        f2.type = 1;
        f2.pid = "f";
        deptList.Add(f2);
        orgList.Add(new SysOrg(f2.id, f2.name,f2.type));
        AddPostionDept(deptList, orgList, "f2");

        SysOrgDept f3 = new SysOrgDept();
        f3.id = "f3";
        f3.name = "摩洛哥";
        f3.label = "1";
        f3.ornum = 3;
        f3.avtag = true;
        f3.tier = "x"+rootId+"xfxf3x";
        f3.type = 1;
        f3.pid = "f";
        deptList.Add(f3);
        orgList.Add(new SysOrg(f3.id, f3.name,f3.type));
        AddPostionDept(deptList, orgList, "f3");

        SysOrgDept f4 = new SysOrgDept();
        f4.id = "f4";
        f4.name = "克罗地亚";
        f4.label = "1";
        f4.ornum = 4;
        f4.avtag = true;
        f4.tier = "x"+rootId+"xfxf4x";
        f4.type = 1;
        f4.pid = "f";
        deptList.Add(f4);
        orgList.Add(new SysOrg(f4.id, f4.name,f4.type));
        AddPostionDept(deptList, orgList, "f4");

        SysOrgDept g = new SysOrgDept();
        g.id = "g";
        g.name = "G组_巴塞瑞喀";
        g.label = "1";
        g.ornum = 7;
        g.avtag = true;
        g.tier = "x"+rootId+"xgx";
        g.type = 1;
        g.pid = rootId;
        deptList.Add(g);
        orgList.Add(new SysOrg(g.id, g.name,g.type));

        SysOrgDept g1 = new SysOrgDept();
        g1.id = "g1";
        g1.name = "巴西";
        g1.label = "1";
        g1.ornum = 1;
        g1.avtag = true;
        g1.tier = "x"+rootId+"xgxg1x";
        g1.type = 1;
        g1.pid = "g";
        deptList.Add(g1);
        orgList.Add(new SysOrg(g1.id, g1.name,g1.type));
        AddPostionDept(deptList, orgList, "g1");

        SysOrgDept g2 = new SysOrgDept();
        g2.id = "g2";
        g2.name = "塞尔维亚";
        g2.label = "1";
        g2.ornum = 2;
        g2.avtag = true;
        g2.tier = "x"+rootId+"xgxg2x";
        g2.type = 1;
        g2.pid = "g";
        deptList.Add(g2);
        orgList.Add(new SysOrg(g2.id, g2.name,g2.type));
        AddPostionDept(deptList, orgList, "g2");

        SysOrgDept g3 = new SysOrgDept();
        g3.id = "g3";
        g3.name = "瑞士";
        g3.label = "1";
        g3.ornum = 3;
        g3.avtag = true;
        g3.tier = "x"+rootId+"xgxg3x";
        g3.type = 1;
        g3.pid = "g";
        deptList.Add(g3);
        orgList.Add(new SysOrg(g3.id, g3.name,g3.type));
        AddPostionDept(deptList, orgList, "g3");

        SysOrgDept g4 = new SysOrgDept();
        g4.id = "g4";
        g4.name = "喀麦隆";
        g4.label = "1";
        g4.ornum = 4;
        g4.avtag = true;
        g4.tier = "x"+rootId+"xgxg4x";
        g4.type = 1;
        g4.pid = "g";
        deptList.Add(g4);
        orgList.Add(new SysOrg(g4.id, g4.name,g4.type));
        AddPostionDept(deptList, orgList, "g4");

        SysOrgDept h = new SysOrgDept();
        h.id = "h";
        h.name = "H组_葡加乌韩";
        h.label = "1";
        h.ornum = 8;
        h.avtag = true;
        h.tier = "x"+rootId+"xhx";
        h.type = 1;
        h.pid = rootId;
        deptList.Add(h);
        orgList.Add(new SysOrg(h.id, h.name,h.type));

        SysOrgDept h1 = new SysOrgDept();
        h1.id = "h1";
        h1.name = "葡萄牙";
        h1.label = "1";
        h1.ornum = 1;
        h1.avtag = true;
        h1.tier = "x"+rootId+"xhxh1x";
        h1.type = 1;
        h1.pid = "h";
        deptList.Add(h1);
        orgList.Add(new SysOrg(h1.id, h1.name,h1.type));
        AddPostionDept(deptList, orgList, "h1");

        SysOrgDept h2 = new SysOrgDept();
        h2.id = "h2";
        h2.name = "加纳";
        h2.label = "1";
        h2.ornum = 2;
        h2.avtag = true;
        h2.tier = "x"+rootId+"xhxh2x";
        h2.type = 1;
        h2.pid = "h";
        deptList.Add(h2);
        orgList.Add(new SysOrg(h2.id, h2.name,h2.type));
        AddPostionDept(deptList, orgList, "h2");

        SysOrgDept h3 = new SysOrgDept();
        h3.id = "h3";
        h3.name = "乌拉圭";
        h3.label = "1";
        h3.ornum = 3;
        h3.avtag = true;
        h3.tier = "x"+rootId+"xhxh3x";
        h3.type = 1;
        h3.pid = "h";
        deptList.Add(h3);
        orgList.Add(new SysOrg(h3.id, h3.name,h3.type));
        AddPostionDept(deptList, orgList, "h3");

        SysOrgDept h4 = new SysOrgDept();
        h4.id = "h4";
        h4.name = "韩国";
        h4.label = "1";
        h4.ornum = 4;
        h4.avtag = true;
        h4.tier = "x"+rootId+"xhxh4x";
        h4.type = 1;
        h4.pid = "h";
        deptList.Add(h4);
        orgList.Add(new SysOrg(h4.id, h4.name,h4.type));
        AddPostionDept(deptList, orgList, "h4");

//        SysOrgDept ec1=new SysOrgDept();
//        ec1.setId("ec1");
//        ec1.setName("客户");
//        ec1.setLabel("ec1");
//        ec1.setOrnum(1);
//        ec1.setAvtag(true);
//        ec1.setTier("xec1x");
//        deptList.add(ec1);
//
//        SysOrgDept ec2=new SysOrgDept();
//        ec2.setId("ec2");
//        ec2.setName("渠道商");
//        ec2.setLabel("ec2");
//        ec2.setOrnum(2);
//        ec2.setAvtag(true);
//        ec2.setTier("xec2x");
//        deptList.add(ec2);
//
//        SysOrgDept ec3=new SysOrgDept();
//        ec3.setId("ec3");
//        ec3.setName("供应商");
//        ec3.setLabel("ec3");
//        ec3.setOrnum(3);
//        ec3.setAvtag(true);
//        ec3.setTier("xec3x");
//        deptList.add(ec3);

        await _deptRepo.InsertRangeAsync(deptList);
        await _orgRepo.InsertRangeAsync(orgList);
    }

    //公司及子公司初始化


    //管理员初始化，正式项目一般只初始化这个就够了，上面的都是演示数据
    public async Task InitSa()
    {
        Console.WriteLine("开始初始化数据");
        SysOrgUser user = new SysOrgUser();
        user.id = "sa";
        user.usnam = "sa";
        user.name = "管理员";
        user.pacod = SecureUtils.PasswordEncrypt("1");
        user.avtag = true;
        user.depid = rootId;
        user.tier = "x"+rootId+"xsax";
        user.crtim = DateTime.Now;
        await _userRepo.InsertAsync(user);
        await _orgRepo.InsertAsync(new SysOrg("sa", "管理员",8));

        SysOrgUser user2 = new SysOrgUser();
        user2.id = "vben";
        user2.usnam = "vben";
        user2.name = "小维";
        user2.pacod = SecureUtils.PasswordEncrypt("123456");
        user2.avtag = true;
        user2.depid = rootId;
        user2.tier = "x"+rootId+"xvbenx";
        user2.crtim = DateTime.Now;
        await _userRepo.InsertAsync(user2);
        await _orgRepo.InsertAsync(new SysOrg("vben", "小维",8));
    }

    private void AddPostionDept(List<SysOrgDept> deptList, List<SysOrg> orgList, string id)
    {
        SysOrgDept qf = new SysOrgDept();
        qf.id = id + "qf";
        qf.name = id.Substring(0, 1).ToUpper() + id.Substring(1) + "_前锋";
        qf.label = "1";
        qf.ornum = 1;
        qf.avtag = true;
        qf.tier = "x"+rootId+"x" + id.Substring(0, 1) + "x" + id + "x" + id + "qf" + "x";
        qf.type = 2;
        qf.pid = id;
        deptList.Add(qf);
        orgList.Add(new SysOrg(qf.id, qf.name,2));
//
        SysOrgDept zc = new SysOrgDept();
        zc.id = id + "zc";
        zc.name = id.Substring(0, 1).ToUpper() + id.Substring(1) + "_中场";
        zc.label = "1";
        zc.ornum = 2;
        zc.avtag = true;
        zc.tier = "x"+rootId+"x" + id.Substring(0, 1) + "x" + id + "x" + id + "zc" + "x";
        zc.type = 2;
        zc.pid = id;
        deptList.Add(zc);
        orgList.Add(new SysOrg(zc.id, zc.name,2));
//
        SysOrgDept hw = new SysOrgDept();
        hw.id = id + "hw";
        hw.name = id.Substring(0, 1).ToUpper() + id.Substring(1) + "_后卫";
        hw.label = "1";
        hw.ornum = 3;
        hw.avtag = true;
        hw.tier = "x"+rootId+"x" + id.Substring(0, 1) + "x" + id + "x" + id + "hw" + "x";
        hw.type = 2;
        hw.pid = id;
        deptList.Add(hw);
        orgList.Add(new SysOrg(hw.id, hw.name,2));
//
        SysOrgDept mj = new SysOrgDept();
        mj.id = id + "mj";
        mj.name = id.Substring(0, 1).ToUpper() + id.Substring(1) + "_门将";
        mj.label = "1";
        mj.ornum = 4;
        mj.avtag = true;
        mj.tier = "x"+rootId+"x" + id.Substring(0, 1) + "x" + id + "x" + id + "mj" + "x";
        mj.type = 2;
        mj.pid = id;
        deptList.Add(mj);
        orgList.Add(new SysOrg(mj.id, mj.name,2));
    }


    public async Task initTestOrg()
    {
        SysOrgDept d1 = new SysOrgDept();
        d1.id = "dept1";
        d1.name = "测试一部";
        d1.label = "1";
        d1.ornum = 9991;
        d1.avtag = true;
        d1.tier = "x"+rootId+"xdept1x";
        d1.type = 2;
        d1.pid = rootId;
        await _deptRepo.InsertAsync(d1);
        await _orgRepo.InsertAsync(new SysOrg(d1.id, d1.name,2));
        
        var userList = new List<SysOrgUser>();
        var orgList = new List<SysOrg>();

        SysOrgUser u1 = new SysOrgUser();
        u1.id = "l1";
        u1.usnam = "l1";
        u1.name = "刘一";
        u1.pacod = SecureUtils.PasswordEncrypt("1");
        u1.ornum = 1;
        u1.avtag = true;
        u1.depid = "dept1";
        u1.tier = "x"+rootId+"xdept1xl1x";
        userList.Add(u1);
        orgList.Add(new SysOrg(u1.id, u1.name,8));

        SysOrgUser u2 = new SysOrgUser();
        u2.id = "c2";
        u2.usnam = "c2";
        u2.name = "陈二";
        u2.pacod = SecureUtils.PasswordEncrypt("1");
        u2.ornum = 2;
        u2.avtag = true;
        u2.depid = "dept1";
        u2.tier = "x"+rootId+"xdept1xc2x";
        userList.Add(u2);
        orgList.Add(new SysOrg(u2.id, u2.name,8));

        SysOrgUser u3 = new SysOrgUser();
        u3.id = "z3";
        u3.usnam = "z3";
        u3.name = "张三";
        u3.pacod = SecureUtils.PasswordEncrypt("1");
        u3.ornum = 3;
        u3.avtag = true;
        u3.depid = "dept1";
        u3.tier = "x"+rootId+"xdept1xz3x";
        userList.Add(u3);
        orgList.Add(new SysOrg(u3.id, u3.name,8));

        SysOrgUser u4 = new SysOrgUser();
        u4.id = "l4";
        u4.usnam = "l4";
        u4.name = "李四";
        u4.pacod = SecureUtils.PasswordEncrypt("1");
        u4.ornum = 4;
        u4.avtag = true;
        u4.depid = "dept1";
        u4.tier = "x"+rootId+"xdept1xl4x";
        userList.Add(u4);
        orgList.Add(new SysOrg(u4.id, u4.name,8));

        SysOrgDept d2 = new SysOrgDept();
        d2.id = "dept2";
        d2.name = "测试二部";
        d2.label = "1";
        d2.ornum = 9992;
        d2.avtag = true;
        d2.tier = "x"+rootId+"xdept2x";
        d2.type = 2;
        d2.pid = rootId;
        await _deptRepo.InsertAsync(d2);
        await _orgRepo.InsertAsync(new SysOrg(d2.id, d2.name,2));
        
        
        SysOrgUser u5 = new SysOrgUser();
        u5.id = "w5";
        u5.usnam = "w5";
        u5.name = "王五";
        u5.pacod = SecureUtils.PasswordEncrypt("1");
        u5.ornum = 5;
        u5.avtag = true;
        u5.depid = "dept2";
        u5.tier = "x"+rootId+"xdept2xw5x";
        userList.Add(u5);
        orgList.Add(new SysOrg(u5.id, u5.name,8));

        SysOrgUser u6 = new SysOrgUser();
        u6.id = "z6";
        u6.usnam = "z6";
        u6.name = "赵六";
        u6.pacod = SecureUtils.PasswordEncrypt("1");
        u6.ornum = 6;
        u6.avtag = true;
        u6.depid = "dept2";
        u6.tier = "x"+rootId+"xdept2xz6x";
        userList.Add(u6);
        orgList.Add(new SysOrg(u6.id, u6.name,8));

        SysOrgUser u7 = new SysOrgUser();
        u7.id = "s7";
        u7.usnam = "s7";
        u7.name = "孙七";
        u7.pacod = SecureUtils.PasswordEncrypt("1");
        u7.ornum = 7;
        u7.avtag = true;
        u7.depid = "dept2";
        u7.tier = "x"+rootId+"xdept2xs7x";
        userList.Add(u7);
        orgList.Add(new SysOrg(u7.id, u7.name,8));

        SysOrgUser u8 = new SysOrgUser();
        u8.id = "z8";
        u8.usnam = "z8";
        u8.name = "周八";
        u8.pacod = SecureUtils.PasswordEncrypt("1");
        u8.ornum = 8;
        u8.avtag = true;
        u8.depid = "dept2";
        u8.tier = "x"+rootId+"xdept2xz8x";
        userList.Add(u8);
        orgList.Add(new SysOrg(u8.id, u8.name,8));

        SysOrgPost p1 = new SysOrgPost();
        p1.id = "post1";
        p1.name = "一部负责人";
        p1.ornum = 0;
        p1.avtag = true;
        p1.depid = "dept1";
        p1.tier = "x"+rootId+"xdept1xpost1x";
        p1.users = new List<SysOrg>();
        p1.users.Add(new SysOrg("l1","刘一",8));
        orgList.Add(new SysOrg(p1.id, p1.name,4));
        
        SysOrgPost p2 = new SysOrgPost();
        p2.id = "post2";
        p2.name = "二部负责人";
        p2.ornum = 0;
        p2.avtag = true;
        p2.depid = "dept2";
        p2.tier = "x"+rootId+"xdept2xpost2x";
        p2.users = new List<SysOrg>();
        p2.users.Add(new SysOrg("w5","王五",8));
        p2.users.Add(new SysOrg("z6","赵六",8));
        orgList.Add(new SysOrg(p2.id, p2.name,4));
        
        SysOrgGroupCate gcate = new SysOrgGroupCate();
        gcate.id = "gcate";
        gcate.name = "测试群组分类";
        gcate.avtag = true;
        
        SysOrgGroup g1 = new SysOrgGroup();
        g1.id = "group1";
        g1.name = "张三李四组";
        g1.ornum = 1;
        g1.avtag = true;
        g1.catid = "gcate";
        g1.members = new List<SysOrg>();
        g1.members.Add(new SysOrg("z3","张三",8));
        g1.members.Add(new SysOrg("l4","李四",8));
        orgList.Add(new SysOrg(g1.id, g1.name,16));
        
        SysOrgGroup g2 = new SysOrgGroup();
        g2.id = "group2";
        g2.name = "用户岗位部门混合组";
        g2.ornum = 2;
        g2.avtag = true;
        g2.catid = "gcate";
        g2.members = new List<SysOrg>();
        g2.members.Add(new SysOrg("c2","陈二",8));
        g2.members.Add(new SysOrg("post1","一部负责人",4));
        g2.members.Add(new SysOrg("dept2","测试二部",2));
        orgList.Add(new SysOrg(g2.id, g2.name,16));
        
        
        await _userRepo.InsertRangeAsync(userList);
        await _orgRepo.InsertRangeAsync(orgList);
       
        await _postRepo.Context.InsertNav(p1).Include(it => it.users).ExecuteCommandAsync();
        await _postRepo.Context.InsertNav(p2).Include(it => it.users).ExecuteCommandAsync();
        await _groupRepo.Context.Insertable(gcate).ExecuteCommandAsync();
        await _groupRepo.Context.InsertNav(g1).Include(it => it.members).ExecuteCommandAsync();
        await _groupRepo.Context.InsertNav(g2).Include(it => it.members).ExecuteCommandAsync();
        
        SysOrgRoleTree rtree1 = new SysOrgRoleTree();
        rtree1.id = "rtree1";
        rtree1.name = "业务角色树";
        rtree1.ornum = 1;
        rtree1.avtag = true;
        rtree1.roles = new List<SysOrgRole>();
        rtree1.roles.Add(new SysOrgRole("role1","分管领导",1));
        rtree1.roles.Add(new SysOrgRole("role2","部门领导",2));
        await _roleTreeService.Insertx(rtree1);
        
        SysOrgRoleNode rnode1 = new SysOrgRoleNode();
        rnode1.id = "rnode1";
        rnode1.name = "一部分管领导";
        rnode1.treid = "rtree1";
        rnode1.ornum = 1;
        rnode1.avtag = true;
        rnode1.memid = "post1";
        rnode1.tier = "xrnode1x";
        await _roleNodeRepo.InsertAsync(rnode1);
        
        SysOrgRoleNode rnode11 = new SysOrgRoleNode();
        rnode11.id = "rnode11";
        rnode11.name = "一部部门领导";
        rnode11.treid = "rtree1";
        rnode11.ornum = 1;
        rnode11.avtag = true;
        rnode11.memid = "c2";
        rnode11.pid = "rnode1";
        rnode11.tier = "xrnode1xrnode11x";
        await _roleNodeRepo.InsertAsync(rnode11);
        
        SysOrgRoleNode rnode111 = new SysOrgRoleNode();
        rnode111.id = "rnode111";
        rnode111.name = "一部人员";
        rnode111.treid = "rtree1";
        rnode111.ornum = 1;
        rnode111.avtag = true;
        rnode111.memid = "dept1";
        rnode111.pid = "rnode11";
        rnode111.tier = "xrnode1xrnode11xrnode111x";
        await _roleNodeRepo.InsertAsync(rnode111);
        
        SysOrgRoleNode rnode2 = new SysOrgRoleNode();
        rnode2.id = "rnode2";
        rnode2.name = "二部分管领导";
        rnode2.treid = "rtree1";
        rnode2.ornum = 2;
        rnode2.avtag = true;
        rnode2.memid = "post2";
        rnode2.tier = "xrnode2x";
        await _roleNodeRepo.InsertAsync(rnode2);
        
        SysOrgRoleNode rnode21 = new SysOrgRoleNode();
        rnode21.id = "rnode21";
        rnode21.name = "二部部门领导";
        rnode21.treid = "rtree1";
        rnode21.ornum = 1;
        rnode21.avtag = true;
        rnode21.memid = "z6";
        rnode21.pid = "rnode2";
        rnode21.tier = "xrnode2xrnode21x";
        await _roleNodeRepo.InsertAsync(rnode21);
        
        SysOrgRoleNode rnode211 = new SysOrgRoleNode();
        rnode211.id = "rnode211";
        rnode211.name = "二部人员";
        rnode211.treid = "rtree1";
        rnode211.ornum = 1;
        rnode211.avtag = true;
        rnode211.memid = "dept2";
        rnode211.pid = "rnode21";
        rnode211.tier = "xrnode2xrnode21xrnode211x";
        await _roleNodeRepo.InsertAsync(rnode211);

    }
}