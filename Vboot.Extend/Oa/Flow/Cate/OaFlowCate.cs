﻿namespace Vboot.Extend.Oa;

[SugarTable("oa_flow_cate", TableDescription = "OA流程分类表")]
[Description("OA流程分类表")]
public class OaFlowCate : BaseMainEntity
{
    [SugarColumn(ColumnDescription = "排序号", IsNullable = true)]
    public int ornum { get; set; }

    [SugarColumn(ColumnDescription = "父ID", IsNullable = true, Length = 32)]
    public string pid { get; set; }

    [SqlSugar.SugarColumn(IsIgnore = true)]
    public string panam { get; set; }
    
    [SqlSugar.SugarColumn(IsIgnore = true)]
    public OaFlowCate parent { get; set; }

    [SqlSugar.SugarColumn(IsIgnore = true)]
    public List<OaFlowCate> children { get; set; }
}