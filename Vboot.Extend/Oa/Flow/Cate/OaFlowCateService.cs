﻿namespace Vboot.Extend.Oa;

public class OaFlowCateService : BaseMainService<OaFlowCate>, ITransient
{
    public OaFlowCateService(SqlSugarRepository<OaFlowCate> repo)
    {
        this.repo = repo;
    }
}