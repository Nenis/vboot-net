﻿using Vboot.Core.Module.Bpm;

namespace Vboot.Extend.Oa;

public class OaFlowMainService : BaseMainService<OaFlowMain>, ITransient
{
    public async Task Insertx(OaFlowMain oaFlowMain)
    {
        oaFlowMain.state = "20";
        oaFlowMain.uptim = DateTime.Now;
        await InsertAsync(oaFlowMain);
        Zbpm zbpm = oaFlowMain.zbpm;
        zbpm.modty = "oaFlow";
        zbpm.proid = oaFlowMain.id;
        zbpm.prona = oaFlowMain.name;
        zbpm.haman = oaFlowMain.crmid;
        zbpm.tmpid = oaFlowMain.protd;
        zbpm.crman = oaFlowMain.crmid;
        Znode znode = await _procService.Start(zbpm);
        if (znode.facno == "NE")
        {
            oaFlowMain.state = "30";
            await UpdateAsync(oaFlowMain);
        }
    }

    public async Task Updatex(OaFlowMain oaFlowMain)
    {
        await UpdateAsync(oaFlowMain);
        oaFlowMain.zbpm.modty = "oaFlow";
        oaFlowMain.zbpm.crman = oaFlowMain.crmid;
        if (oaFlowMain.zbpm.opkey == "pass")
        {
            Znode znode =await _procService.HandlerPass(oaFlowMain.zbpm);
            if (znode.facno == "NE")
            {
                oaFlowMain.state = "30";
                await UpdateAsync(oaFlowMain);
            }
            else
            {
                oaFlowMain.state = "20";
                await UpdateAsync(oaFlowMain);
            }
        }
        else if (oaFlowMain.zbpm.opkey == "refuse")
        {
            if ("N1"==oaFlowMain.zbpm.tarno) {
                oaFlowMain.state="11";
                await UpdateAsync(oaFlowMain);
            }
            await _procService.HandlerRefuse(oaFlowMain.zbpm);
        }else if (oaFlowMain.zbpm.opkey == "turn") {
            await _procService.handlerTurn(oaFlowMain.zbpm);
        } else if (oaFlowMain.zbpm.opkey == "communicate") {
            await _procService.handlerCommunicate(oaFlowMain.zbpm);
        } else if (oaFlowMain.zbpm.opkey == "abandon") {
            await _procService.handlerAbandon(oaFlowMain.zbpm);
            oaFlowMain.state="00";
            await UpdateAsync(oaFlowMain);
        }else if (oaFlowMain.zbpm.opkey == "bacommunicate") {
            await _procService.handlerBacommunicate(oaFlowMain.zbpm);
        }else if (oaFlowMain.zbpm.opkey == "cacommunicate") {
            await _procService.handlerCacommunicate(oaFlowMain.zbpm);
        }
    }

    private readonly BpmProcMainService _procService;


    public OaFlowMainService(SqlSugarRepository<OaFlowMain> repo,
        BpmProcMainService procService)
    {
        this.repo = repo;
        _procService = procService;
    }
}