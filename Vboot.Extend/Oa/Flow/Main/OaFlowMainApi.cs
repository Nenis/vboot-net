﻿using Furion;
using Vboot.Core.Module.Bpm;

namespace Vboot.Extend.Oa;

[ApiDescriptionSettings("Ext", Tag = "流程管理-流程实例")]
public class OaFlowMainApi : IDynamicApiController
{
    private readonly OaFlowMainService _service;

    private readonly BpmTaskMainService _taskMainService;

    public OaFlowMainApi(OaFlowMainService service,
        BpmTaskMainService taskMainService)
    {
        _service = service;
        _taskMainService = taskMainService;
    }

    [QueryParameters]
    public async Task<dynamic> Get(string name)
    {
        var pp = XreqUtil.GetPp();
        var items = new List<dynamic>();
        var dbOptions = App.GetOptions<ConnectionStringsOptions>();

        if (dbOptions.ConnectionConfigs[0].DbType == DbType.Oracle)
        {
            var sql = "select t.id \"id\",t.name \"name\",t.notes \"notes\",c.name \"temna\",o.name \"crman\"," +
                      "t.crtim \"crtim\",t.uptim \"uptim\",t.state \"state\" " +
                      "from oa_flow_main t left join oa_flow_tmpl c on c.id=t.tmpid " +
                      "left join sys_org o on o.id=t.crmid ";
            if (!string.IsNullOrEmpty(name))
            {
                sql += " where t.name like @name";
            }

            sql += " order by t.crtim desc";
      
            if (!string.IsNullOrEmpty(name))
            {
                items = await _service.repo.Context.SqlQueryable<dynamic>(sql)
                    .AddParameters(new SugarParameter[] {new SugarParameter("@name", "%"+name+"%")})
                    .ToPageListAsync(pp.page, pp.pageSize, pp.total);
            }
            else
            {
                items = await _service.repo.Context.SqlQueryable<dynamic>(sql)
                    .ToPageListAsync(pp.page, pp.pageSize, pp.total);
            }
        }
        else
        {
           items = await _service.repo.Context.Queryable<OaFlowMain, OaFlowTmpl, SysOrg>((t, c, o)
                    => new JoinQueryInfos(JoinType.Left, c.id == t.tmpid, JoinType.Left, o.id == t.crmid))
                .WhereIF(!string.IsNullOrWhiteSpace(name), t => t.name.Contains(name.Trim()))
                .OrderBy(t => t.crtim, OrderByType.Desc)
                .Select((t, c, o) =>
                   (dynamic) new {t.id, t.name, t.notes, temna = c.name, crman = o.name, t.crtim, t.uptim, t.state})
            .ToPageListAsync(pp.page, pp.pageSize, pp.total);
        }

        await _taskMainService.FindCurrentExmen(items);
        return RestPageResult.Build(pp.total.Value, items);
    }

    public async Task<OaFlowMain> GetOne(string id)
    {
        var main = await _service.repo.Context.Queryable<OaFlowMain>()
            .Where(it => it.id == id).FirstAsync();
        return main;
    }

    [MyUnitOfWork]
    public async Task Post(OaFlowMain main)
    {
        await _service.Insertx(main);
    }

    [MyUnitOfWork]
    public async Task Put(OaFlowMain cate)
    {
        await _service.Updatex(cate);
    }

    public async Task Delete(string ids)
    {
        await _service.DeleteAsync(ids);
    }
}