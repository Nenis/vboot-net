﻿namespace Vboot.Extend.Oa;

[ApiDescriptionSettings("Ext", Tag = "流程管理-使用记录")]
public class OaFlowReceApi : IDynamicApiController
{
    private readonly OaFlowReceService _service;

    public OaFlowReceApi(OaFlowReceService service)
    {
        _service = service;
    }

    public async Task Post(List<OaFlowRece> reces)
    {
        if (reces != null && reces.Count > 0)
        {
          await  _service.update(reces);
        }
    }

    public async Task<dynamic> GetList()
    {
        string userId = XuserUtil.getUserId();
        var list = await _service._repo.Context.Queryable<OaFlowRece, OaFlowTmpl,OaFlowCate>((t, f,c)
                => new JoinQueryInfos(JoinType.Inner, f.id == t.floid,JoinType.Inner, c.id == f.catid))
            .OrderBy(t => t.uptim, OrderByType.Desc)
            .Where(t => t.useid == userId)
            .Select((t, f,c) => new {id = t.floid, f.name,catna=c.name})
            .ToListAsync();
        return list;
    }
}