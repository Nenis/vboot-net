﻿namespace Vboot.Extend.Oa;

public class OaFlowReceService : ITransient
{
    public async Task update(List<OaFlowRece> reces)
    {
        string userId = XuserUtil.getUserId();
        List<string> flowidList = new List<string>();
        foreach (var rece in reces)
        {
            rece.floid = rece.id;
            rece.id = YitIdHelper.NextId() + "";
            rece.useid = userId;
            rece.uptim = DateTime.Now;
            flowidList.Add(rece.floid);
        }

        //数据库删除本次已传的记录
        await _repo.Context.Deleteable<OaFlowRece>().Where("useid=@useid",new { useid=userId})
            .Where("floid in (@floids)", new {floids = flowidList.ToArray()}).ExecuteCommandAsync();
        
        //删除当前数据库最近10次前的数据
        RefAsync<int> total = 0;
        var items = await _repo.Context.Queryable<OaFlowRece>()
            .Where(it=>it.useid==userId)
            .OrderBy(u => u.uptim, OrderByType.Desc)
            .Select((t) => t.id)
            .ToPageListAsync(2, 10, total);
        if (items.Count > 0)
        {
            await _repo.Context.Deleteable<OaFlowRece>().In(items.ToArray()).ExecuteCommandAsync();
        }

        //插入本次使用的记录
        await _repo.InsertRangeAsync(reces);
        
    }


    public readonly SqlSugarRepository<OaFlowRece> _repo;

    public OaFlowReceService(SqlSugarRepository<OaFlowRece> repo)
    {
        _repo = repo;
    }
}